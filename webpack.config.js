const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'app', 'resources', 'js', 'index.jsx'),
  // resolve: {
  //   extensions: ['.js', '.jsx']
  // },
  output: {
    filename: 'bundle.jsx',
    path: path.resolve(__dirname, 'www', 'assets', 'js'),
    publicPath: '/assets'
  },
  //fixme + add hot
  devServer: {
    static: path.resolve(__dirname, 'www', 'assets', 'js'),
    open: true,
    port: 8000,
  },
  module: {
    rules: [
      {
        test: /\.(jsx|js)$/,
        include: path.resolve(__dirname, 'app', 'resources', 'js'),
        exclude: path.resolve(__dirname, 'node_modules'),
        use: [{
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', {
                "targets": "defaults"
              }],
              '@babel/preset-react'
            ]
          }
        }]
      }
    ]
  },
};
