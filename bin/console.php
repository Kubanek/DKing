<?php declare(strict_types=1);

$container = require_once __DIR__ . '/../app/bootstrap.php';

$container->getByType(Contributte\Console\Application::class)->run();
