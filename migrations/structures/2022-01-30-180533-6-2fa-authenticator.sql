CREATE TABLE g2fa (id VARCHAR(25) NOT NULL, user_account_id VARCHAR(255) DEFAULT NULL, last_edited_by_id VARCHAR(255) DEFAULT NULL, secret VARCHAR(255) NOT NULL, date_created DATETIME NOT NULL, date_updated DATETIME NOT NULL, UNIQUE INDEX UNIQ_A948BCB73C0C9956 (user_account_id), INDEX IDX_A948BCB7D48D54E8 (last_edited_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;
ALTER TABLE g2fa ADD CONSTRAINT FK_A948BCB73C0C9956 FOREIGN KEY (user_account_id) REFERENCES user_account (id) ON DELETE CASCADE;
ALTER TABLE g2fa ADD CONSTRAINT FK_A948BCB7D48D54E8 FOREIGN KEY (last_edited_by_id) REFERENCES user_account (id) ON DELETE SET NULL;

ALTER TABLE user_account ADD last_edited_by_id VARCHAR(255) DEFAULT NULL, CHANGE id id VARCHAR(25) NOT NULL;
ALTER TABLE user_account ADD CONSTRAINT FK_253B48AED48D54E8 FOREIGN KEY (last_edited_by_id) REFERENCES user_account (id) ON DELETE SET NULL;

INSERT INTO `g2fa` (`id`, `user_account_id`, `last_edited_by_id`, `secret`, `date_created`, `date_updated`) VALUES
('0000000001-llf3ui01cf',	'0000000001-yk30rq8i1d',	'0000000001-yk30rq8i1d',	'SlM3Slg3T0NWRzNNTzZUNTdWNVNXUEVCTU9RVk03SllQMkU0T0lKSkVaVlAzRDVLTENDU1VVTEZERlBTV1lMNQ==',	'2022-01-30 18:35:15',	'2022-01-30 18:35:15');

INSERT INTO `white_list` (`id`, `user_account_id`, `ip_address`, `is_main`, `last_edited_by_id`, `sign_in_allowed`, `date_created`, `date_updated`) VALUES
('0000000001-kf53juezkp',	'0000000001-yk30rq8i1d',	'127.0.0.1',	0,	'0000000001-yk30rq8i1d',	1,	now(),	now());
