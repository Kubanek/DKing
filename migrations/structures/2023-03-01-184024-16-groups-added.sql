CREATE TABLE message_group (id VARCHAR(25) NOT NULL, last_edited_by_id VARCHAR(25) DEFAULT NULL, date_created DATETIME NOT NULL, date_updated DATETIME NOT NULL, name VARCHAR(255) DEFAULT NULL, INDEX IDX_9266BB2ED48D54E8 (last_edited_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;
CREATE TABLE user_message_groups (message_group_id VARCHAR(25) NOT NULL, user_id VARCHAR(25) NOT NULL, INDEX IDX_ACCF7B26F7721D56 (message_group_id), INDEX IDX_ACCF7B26A76ED395 (user_id), PRIMARY KEY(message_group_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;
CREATE TABLE message (id VARCHAR(25) NOT NULL, user_account_id VARCHAR(25) DEFAULT NULL, message_group_id VARCHAR(25) DEFAULT NULL, text LONGTEXT NOT NULL, date_created DATETIME NOT NULL, date_updated DATETIME NOT NULL, INDEX IDX_B6BD307F3C0C9956 (user_account_id), INDEX IDX_B6BD307FF7721D56 (message_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;
ALTER TABLE message_group ADD CONSTRAINT FK_9266BB2ED48D54E8 FOREIGN KEY (last_edited_by_id) REFERENCES user_account (id) ON DELETE SET NULL;
ALTER TABLE message_group ADD is_main TINYINT(1) NOT NULL;
ALTER TABLE user_message_groups ADD CONSTRAINT FK_ACCF7B26F7721D56 FOREIGN KEY (message_group_id) REFERENCES message_group (id);
ALTER TABLE user_message_groups ADD CONSTRAINT FK_ACCF7B26A76ED395 FOREIGN KEY (user_id) REFERENCES user_account (id);
ALTER TABLE message ADD CONSTRAINT FK_B6BD307F3C0C9956 FOREIGN KEY (user_account_id) REFERENCES user_account (id) ON DELETE SET NULL;
ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF7721D56 FOREIGN KEY (message_group_id) REFERENCES message_group (id) ON DELETE SET NULL;

INSERT INTO `message_group` (`id`, `last_edited_by_id`, `date_created`, `date_updated`, `name`, `is_main`) VALUES ('0000000001-149bva1w79', '0000000001-yk30rq8i1d', '2023-03-01 19:42:07', '2023-03-01 19:42:07',	'Obecné', true);
INSERT INTO `user_message_groups` (`message_group_id`, `user_id`) VALUES ('0000000001-149bva1w79',	'0000000001-yk30rq8i1d');
INSERT INTO `message` (`id`, `user_account_id`, `message_group_id`, `text`, `date_created`, `date_updated`) VALUES ('0000000001-27btekf4l0','0000000001-yk30rq8i1d','0000000001-149bva1w79', 'Testovací zpráva', '2023-03-01 20:40:43','2023-03-01 20:40:43');