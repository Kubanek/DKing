CREATE TABLE `sign_log` (
  id VARCHAR(25) NOT NULL,
  `date_created` datetime NOT NULL,
  `email` varchar(255) NOT NULL
) CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;

CREATE TABLE user_account (id VARCHAR(25) NOT NULL, date_created DATETIME NOT NULL, date_updated DATETIME NOT NULL, name VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;

ALTER TABLE cron_log ADD manual_launched_by VARCHAR(25) DEFAULT NULL, ADD PRIMARY KEY (id);
ALTER TABLE cron_log ADD CONSTRAINT FK_7C0163B3D9773683 FOREIGN KEY (manual_launched_by) REFERENCES user_account (id);

ALTER TABLE sign_log ADD ip_address VARCHAR(255) DEFAULT NULL;

ALTER TABLE user_account ADD password VARCHAR(255) NOT NULL;
ALTER TABLE user_account ADD password_gain_by_system VARCHAR(255) DEFAULT NULL;
ALTER TABLE user_account ADD surname VARCHAR(255) DEFAULT NULL AFTER `name`;
ALTER TABLE user_account ADD is_admin VARCHAR(255) DEFAULT NULL AFTER `password_gain_by_system`;

INSERT INTO `user_account` (`id`, `date_created`, `date_updated`, `name`, `email`, `surname`, `password`, `password_gain_by_system`, `is_admin`) VALUES
('0000000001-yk30rq8i1d',	now(),	now(),	'Admin',	'admin@test.cz', 'DK',	'$2y$10$n6dHZdpShvhcbOqq9jBURuxT/SHpIWTz3fuXYbcodyzqhwcGdE/m6',	'$2y$10$n6dHZdpShvhcbOqq9jBURuxT/SHpIWTz3fuXYbcodyzqhwcGdE/m6', true);
