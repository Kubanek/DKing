CREATE TABLE `cron_log` (
  `id` varchar(25) NOT NULL,
  `name` varchar(255) NULL,
  `date_created` datetime NOT NULL,
  `duration_in_seconds` int NULL
) CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB ;
