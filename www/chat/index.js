const express = require('express');

const app = express();
const http = require('http');

const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
  console.log('uset logged in');

  socket.on('disconnect', () => {
    console.log('user disconnect');
  })

  socket.on('chat message', (msg) => {
    io.emit('chat message sent', msg);
  })
})

server.listen(3000, () => {
  // document.getElementById(); in future
  console.log('listening on *:3000');
  // console.log(__dirname);
})
