**DKing** - web project
=
Requirements
------------

- Web Project for Nette ^3.1 requires PHP ^8.2

Installation
------------

- To access this project, download it through this [link](https://gitlab.com/Kubanek/DKing).

- Firstly in \app\config directory copy `config.sample.neon`, paste it in and rename to `config.local.neon`
- Then use thoose commands to set up project on your local:
```
    composer install
    mkdir temp/ log/
    npm install
    npm run-script start
    composer mr
```
- If permission are needed for files temp and log paste in console `sudo chmod -R 777 temp/` and `sudo chmod -R 777 log/`
- Then in console paste `php bin/console.php migrations:reset` which will reset migrations (create DB)

### Migration commands
- `php bin/console.php migrations:create s rename-email-column`
- `php bin/console.php migrations:continue`
- `php bin/console.php migrations:reset`
