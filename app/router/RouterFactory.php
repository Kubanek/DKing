<?php

declare(strict_types=1);

namespace App\Router;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{

	public static function createRouter(): RouteList
	{
		$router = new RouteList();

		$adminRouter = $router[] = new RouteList('Admin');
		$adminRouter[] = new Route('a[dmin]/<presenter>/<action>[/<id>]', 'Admins:default');

        $cronRouter = $router[] = new RouteList('Cron');
        $cronRouter[] = new Route('cron/<presenter>/<action=default>[/<id>]', 'Cron:');

        $messageRouter = $router[] = new RouteList('Messenger');
        $messageRouter[] = new Route('m[essenger]/<presenter>/<action>[/<id>]', 'Messages:default');

        $frontRouter = $router[] = new RouteList('Front');
		$frontRouter[] = new Route('/<presenter>/<action>[/<id>]', 'Homepage:default');

        return $router;
	}
}
