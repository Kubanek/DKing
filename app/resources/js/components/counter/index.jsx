import React, { Component } from "react";
import ReactDOM from "react-dom";
import Counter from "./counter.jsx";

const elementId = document.getElementById("findMe");

if (elementId) {
  ReactDOM.render(<Counter/>, document.getElementById("findMe"))
}
