<?php declare(strict_types=1);

namespace DKing\Base\Module;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Contributte\Translation\Translator;

abstract class BaseForm extends Control
{

    protected FormFactory $formFactory;
    protected Translator $translator;

    public array $onSend = [];

    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
        $this->translator = $formFactory->getTranslator();
    }

    public function createForm(): Form
    {
        $form = $this->formFactory->createForm();

        $form->onSuccess[] = [$this, 'formValuesProcessing'];

        return $form;
    }

}
