<?php declare(strict_types=1);


namespace DKing\Base\Module;

use Contributte\Translation\Translator;
use Nette\Application\UI\Form;
use Nette\Application\UI\Template;

/** @property Template $template */
class FormFactory
{

    private Translator $transaltor;

    public function __construct(Translator $translator)
    {
        $this->transaltor = $translator;
    }

    //////////////////////////////////////////////////////// Create

    public function createForm(): Form
    {
        $form = new Form();

        $form->setTranslator($this->transaltor);

        return $form;
    }

    //////////////////////////////////////////////////////// Public

    public function getTranslator(): Translator
    {
        return $this->transaltor;
    }

}
