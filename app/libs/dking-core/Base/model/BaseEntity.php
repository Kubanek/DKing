<?php declare(strict_types=1);

namespace DKing\Base\Model;

use App\Model\Traits\TDateCreated;
use App\Model\Traits\TDateUpdated;
use Doctrine\ORM\Mapping as ORM;
use Nette\SmartObject;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
abstract class BaseEntity
{

    use SmartObject;
    use TDateCreated;
    use TDateUpdated;

    //////////////////////////////////////////////////////// Construct

    public function __construct()
    {
        if (!$this->dateCreated instanceof DateTime) { //when is new entity, set Date Created
            $this->setDateCreated(new DateTime());
        }

        $this->setDateUpdated(new DateTime());
    }

}
