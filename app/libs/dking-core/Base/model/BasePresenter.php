<?php declare(strict_types=1);

namespace App\BaseModule\Presenters;

use App\Model\Entity\UserAccount;
use App\Security\Authenticator;
use App\Security\User;
use App\Service\DevelopmentEnvironmentChecker;
use App\Service\EmailSenderService;
use Contributte\Translation\Translator;
use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette\Application\UI\Presenter;
use Nette\DI\Container;
use Nette\Utils\ArrayHash;

/**
 *@method User getUser()
 */
abstract class BasePresenter extends Presenter
{

    use AutowireProperties;
    use AutowireComponentFactories;

    /** @var Container @inject */
    public Container $context;

    /** @var DevelopmentEnvironmentChecker @inject */
    public DevelopmentEnvironmentChecker $developmentEnvironmentChecker;

    /** @var EmailSenderService @inject */
    public EmailSenderService $emailSenderService;

    /** @var Translator @inject */
    public Translator $translator;

    protected ArrayHash $neon;

    //////////////////////////////////////////////////////// Start up

    public function startup(): void
    {
        parent::startup();

        $this->neon = ArrayHash::from($this->context->getParameters());
    }

    //////////////////////////////////////////////////////// Public

    public function flashMessage($message, string $type = 'info'): \stdClass
    {
        return parent::flashMessage($this->translator->translate($message), $type);
    }

    public function redirectToMessage(): void
    {
        $this->redirect(":Messenger:Messages:");
    }

    public function redirectToAdmin(): void
    {
        $this->redirect(":Admin:Admins:");
    }

    public function redirectToFront(): void
    {
        $this->redirect(":Front:Homepage:");
    }

}
