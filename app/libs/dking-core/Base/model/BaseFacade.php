<?php declare(strict_types=1);

namespace DKing\Base\Model;

use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;
use Nette\SmartObject;
use Nettrine\ORM\EntityManagerDecorator;

/** @SuppressWarnings(NumberOfChildren) */
abstract class BaseFacade
{

    protected EntityManagerDecorator $em;

    protected ObjectRepository $repository;

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em, string $entityClassName)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository($entityClassName);
    }

    //////////////////////////////////////////////////////// Public

    public function find(string $id = null): ?object
    {
        return $id ? $this->repository->find($id) : null;
    }

    public function findAll(): ?array
    {
        return $this->repository->findAll();
    }

    public function findBy(array $criteria, array $order = null, int $limit = null, int $offset = null): ?array
    {
        return $this->repository->findBy($criteria, $order, $limit, $offset);
    }

    public function findOneBy(array $array): ?object
    {
        return $this->repository->findOneBy($array);
    }

    //////////////////////////////////////////////////////// Public

    public function getQueryBuilder(string $alias = 's'): QueryBuilder
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select($alias);
        $qb->from($this->repository->getClassName(), $alias);

        return $qb;
    }

}
