<?php declare(strict_types=1);

namespace DKing\Base\Model;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class IvIdType extends StringType
{

    private const NAME = 'ivid';

    //ivid can be null because of declatration in IVID trait
    public function convertToPHPValue($value, AbstractPlatform $platform): ?string
    {
        if ($value === null || $value === '') {
            return null;
        }

        return (string)$value;
    }

    //ivid can be null because of declatration in IVID trait
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if ($value === null || $value === '') {
            return null;
        }

        return (string)$value;
    }

    //ivid is totally new type -> not found in vendor
    public function getName(): string
    {
        return static::NAME;
    }

}
