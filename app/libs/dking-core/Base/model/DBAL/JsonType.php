<?php declare(strict_types=1);

namespace DKing\Base\Model;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;

class JsonType extends \Doctrine\DBAL\Types\JsonType
{

    //Changed from base method. Json_encode deleted because of wrong data stroring [Better saving is from "array" => E.g.: EmailTemplate]
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw ConversionException::conversionFailedSerialization($value, 'json', json_last_error_msg());
        }

        return "$value";
    }

    //Changed from base method. Json_decode deleted because of wrong data output [Better is string]. E.g.: EmailTemplate
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return null;
        }

        if (is_resource($value)) {
            $value = stream_get_contents($value);
        }

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw ConversionException::conversionFailed($value, $this->getName());
        }

        return $value;
    }

}
