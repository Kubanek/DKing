<?php declare(strict_types=1);

namespace DKing\Base\Model;

use App\Model\Entity\UserAccount;
use Doctrine\ORM\EntityRepository;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Tracy\Debugger;
use Tracy\ILogger;

abstract class BaseRepository extends EntityRepository
{

    private const SET_DATE_UPDATED = 'setDateUpdated';
    private const SET_LAST_EDITED_BY = 'setLastEditedBy';
    private const SET_IS_DELETED = 'setIsDeleted';

    protected EntityManagerDecorator $em;

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em, $classMetadata)
    {
        parent::__construct($em, $classMetadata);

        $this->em = $em;
    }

    //////////////////////////////////////////////////////// Public

    public function save($entity, ?UserAccount $userAccount = null)
    {
        if (method_exists($entity, self::SET_DATE_UPDATED)) {
            $entity->setDateUpdated(new DateTime());
        }

        if ($userAccount && method_exists($entity, self::SET_LAST_EDITED_BY)) {
            $entity->setLastEditedBy($userAccount);
        }

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    public function softDelete($entity, ?UserAccount $userAccount = null): void
    {
        if (!method_exists($entity, self::SET_IS_DELETED)) {
            Debugger::log(
                "{$this->getEntityName($entity)} does not have method setIsDeleted",
                ILogger::ERROR
            );
            return;
        }

        if (method_exists($entity, self::SET_DATE_UPDATED)) {
            $entity->setDateUpdated(new DateTime());
        }

        if (($userAccount instanceOf UserAccount) && method_exists($entity, self::SET_LAST_EDITED_BY)) {
            $entity->setLastEditedBy($userAccount);
        }

        $entity->setIsDeleted(true);

        $this->em->persist($entity);
        $this->em->flush();
    }

    public function deleteDelayed($entity): void
    {
        $this->em->remove($entity);
    }

    public function delete($entity): void
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

}
