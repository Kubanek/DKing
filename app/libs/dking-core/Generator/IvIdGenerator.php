<?php declare(strict_types=1);

namespace DKing\Model\Generator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Nette\Utils\Random;

class IvIdGenerator extends AbstractIdGenerator
{

    private $sequenceName;

    public function __construct($sequenceName = null)
    {
        $this->sequenceName = $sequenceName;
    }

    //////////////////////////////////////////////////////// Public

    public function generate(EntityManager $em, $entity): ?string
    {
        $id = $this->getNextId($em, $entity);
        $uid = $this->generateUid();

        return $id . '-' . $uid;
    }

    public function isPostInsertGenerator(): bool
    {
        return false;
    }

    //////////////////////////////////////////////////////// Private

    private function generateUid(): ?string
    {
        return Random::generate();
    }

    private function getNextId(EntityManager $em, $entity): string
    {
        $tableName = $em->getClassMetadata(get_class($entity))->table['name'];

        $conn = $em->getConnection();
        $sql = "SELECT   `id`, CAST(SUBSTRING_INDEX(`id`, '-', 1) as DECIMAL) AS `id_core`
                FROM     `" . $tableName . "`
                ORDER BY `id_core` DESC";

        // This method finds the highest 'id' in database, if no rows find, then if happens
        if (!$currentHighestId = $conn->executeQuery($sql)->fetchOne()) {
            return str_pad('1', 10, '0', STR_PAD_LEFT);
        }

        // This method finds, if the highest 'id' has '-', when not, if happens
        if (($delimiterIndex = strpos((string)$currentHighestId, '-')) === false) {
            return str_pad((string)($currentHighestId + 1), 10, '0', STR_PAD_LEFT);
        }

        $id = substr($currentHighestId, 0, $delimiterIndex);
        $nextId = (int)$id + 1;

        return str_pad((string)$nextId, 10, '0', STR_PAD_LEFT);
    }

}
