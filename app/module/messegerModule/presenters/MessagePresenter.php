<?php declare(strict_types=1);

namespace App\MessengerModule\Presenters;

use App\BaseModule\Presenters\BasePresenter;
use App\Model\Entity\UserAccount;

abstract class MessagePresenter extends BasePresenter
{

    public UserAccount $userAccount;

    public function startup(): void
    {
        parent::startup();

        $this->isUserLoggedIn();
        $this->userAccount = $this->getUser()->getUserAccount();
    }

    //////////////////////////////////////////////////////// Private

    private function isUserLoggedIn(): void
    {
        $user = $this->getUser();

        if (!$user->isLoggedIn() || !($this->getUser()->getUserAccount() instanceof UserAccount)) {
            $this->flashMessage('youMustSignIn');
            $this->redirect(':Front:Sign:In');
        }

        $this->userAccount = $this->getUser()->getUserAccount();
    }

    //////////////////////////////////////////////////////// Handles

    public function handleRedrawSnippets(): void
    {
        $this->redrawControl('messages');
        $this->redrawControl('groupName');
        $this->redrawControl('sidePanelComponent');
        $this->redrawControl('sidePanel');
    }

}
