<?php declare(strict_types=1);

namespace App\MessengerModule\Presenters;

use App\FrontModule\Components\SettingsForm;
use App\FrontModule\Components\SettingsFormFactory;
use App\MessengerModule\Components\MessageForm;
use App\MessengerModule\Components\MessageFormFactory;
use App\MessengerModule\Components\MessageGroupForm;
use App\MessengerModule\Components\MessageGroupFormFactory;
use App\MessengerModule\Components\MessageGroupSettingsForm;
use App\MessengerModule\Components\MessageGroupSettingsFormFactory;
use App\Model\Entity\Message;
use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use App\Model\Facade\LastReadMessageFacade;
use App\Model\Facade\MessageFacade;
use App\Model\Facade\MessageGroupFacade;
use App\Model\Facade\UserAccountFacade;
use App\Model\Repository\MessageRepository;
use App\Model\Service\UserAccountService;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;

final class MessagesPresenter extends MessagePresenter
{

    public ?MessageGroup $messageGroup;

    /** @var UserAccountFacade @inject */
    public UserAccountFacade $userAccountFacade;

    /** @var MessageFacade @inject */
    public MessageFacade $messageFacade;

    /** @var LastReadMessageFacade @inject */
    public LastReadMessageFacade $lastReadMessageFacade;

    /** @var MessageGroupFacade @inject */
    public MessageGroupFacade $messageGroupFacade;

    /** @var MessageRepository @inject */
    public MessageRepository $messageRepository;

    /** @var UserAccountService @inject */
    public UserAccountService $userAccountService;

    //////////////////////////////////////////////////////// Actions

    public function actionDefault(): void
    {
        $this->template->messageGroup = null;
        $this->template->userAccount = $this->userAccount;
        $this->template->messageGroups = $this->userAccount->getMessageGroupCollection();
        $this->template->messageRepository = $this->messageRepository;
        $this->template->lastReadMessages = $this->lastReadMessageFacade->getLastReadMessagesForGroup(
            $this->userAccount
        );
    }

    public function actionDetail(string $messageGroupId): void
    {
        $this->messageGroup = $this->messageGroupFacade->find($messageGroupId);
        if (!($this->messageGroup instanceof MessageGroup) || !$this->messageGroup->findUserAccount($this->userAccount)) {
            $this->flashMessage('notFound');
            $this->redirect('default');
        }

        $this->template->messageGroup = $this->messageGroup;
        $this->template->userAccount = $this->userAccount;
        $this->template->messageGroups = $this->userAccount->getMessageGroupCollection();
        $this->template->messageRepository = $this->messageRepository;
        $this->template->lastReadMessages = $this->lastReadMessageFacade->getLastReadMessagesForGroup(
            $this->userAccount
        );
    }

    public function actionAddContact(): void
    {
        $this->template->messageGroup = null;
        $this->template->userAccount = $this->userAccount;
        $this->template->messageGroups = $this->userAccount->getMessageGroupCollection();
        $this->template->messageRepository = $this->messageRepository;
        $this->template->lastReadMessages = $this->lastReadMessageFacade->getLastReadMessagesForGroup(
            $this->userAccount
        );
    }

    public function actionSettings(string $id): void
    {
        if ($this->userAccount->getId() !== $id) {
            $this->flashMessage('notAllowed');
            $this->redirect('default');
        }

        $this->template->messageGroup = null;
        $this->template->userAccount = $this->userAccount;
        $this->template->messageGroups = $this->userAccount->getMessageGroupCollection();
        $this->template->messageRepository = $this->messageRepository;
        $this->template->lastReadMessages = $this->lastReadMessageFacade->getLastReadMessagesForGroup(
            $this->userAccount
        );
    }

    public function actionGroupSettings(string $id): void
    {
        $this->messageGroup = $this->messageGroupFacade->find($id);
        if (!($this->messageGroup instanceof MessageGroup)
            || !$this->messageGroup->findUserAccount($this->userAccount)
            || $this->messageGroup->isMain()) {
            $this->flashMessage('notFound');
            $this->redirect('default');
        }

        $this->template->messageGroup = $this->messageGroup;
        $this->template->userAccount = $this->userAccount;
        $this->template->messageGroups = $this->userAccount->getMessageGroupCollection();
        $this->template->messageRepository = $this->messageRepository;
        $this->template->lastReadMessages = $this->lastReadMessageFacade->getLastReadMessagesForGroup(
            $this->userAccount
        );
    }

    public function actionGroupLeave(string $id, string $userAccountId): void
    {
        $this->messageGroup = $this->messageGroupFacade->find($id);
        if (!($this->messageGroup instanceof MessageGroup)
            || $this->userAccount->getId() !== $userAccountId
            || !$this->messageGroup->findUserAccount($this->userAccount)
            || $this->messageGroup->isMain()) {
            $this->flashMessage('notAllowed');
            $this->redirect('default');
        }

        $messageGroup = $this->messageGroupFacade->removeUserAccountFromGroup($this->userAccount, $this->messageGroup);
        if (!$messageGroup->findUserAccount($this->userAccount)) {
            $this->flashMessage("messageGroupSuccessfullyLeftExtended");
        } else {
            $this->flashMessage("messageGroupSuccessfullyLeft");
        }

        $this->redirect("default");
    }

    //////////////////////////////////////////////////////// Handles

    public function handleLastReadMessageChange(): void
    {
        $post = $this->presenter->getRequest()?->getPost();
        if (is_null($post) || count($post) <= 0) {
            return;
        }

        $messageGroupId = $post['messageGroupId'];
        $userAccountId = $post['userAccountId'];

        $userAccount = $this->userAccountFacade->find($userAccountId);
        $messageGroup = $this->messageGroupFacade->find($messageGroupId);
        if (!($messageGroup instanceof MessageGroup) || !($userAccount instanceof UserAccount)) {
            return;
        }

        /** @var $lastMessage Message */
        $lastMessage = $this->messageRepository->getLastMessageByMessageGroupId($messageGroupId);
        $this->lastReadMessageFacade->createAndSave($userAccount, $messageGroup, $lastMessage);

        $this->template->lastReadMessages = $this->lastReadMessageFacade->getLastReadMessagesForGroup(
            $this->userAccount
        );

        $this->redrawControl('messages');
        $this->redrawControl('groupName');
        $this->redrawControl('sidePanelComponent');
        $this->redrawControl('sidePanel');
    }

    //////////////////////////////////////////////////////// Create

    protected function createComponentMessageForm(MessageFormFactory $messageFormFactory): MessageForm
    {
        $form = $messageFormFactory->create($this->userAccount, $this->messageGroup);

        $form->onSend[] = function (UserAccount $userAccount, MessageGroup $messageGroup, string $text) {
            $now = new DateTime();
            $message = $this->messageFacade->createAndSave($userAccount, $messageGroup, $now, $text);
            $this->messageGroupFacade->updateMessageGroupLastEditedBy($userAccount, $messageGroup);
            $this->lastReadMessageFacade->createAndSave($userAccount, $messageGroup, $message);

            $this->template->lastReadMessages = $this->lastReadMessageFacade->getLastReadMessagesForGroup(
                $this->userAccount
            );

            $this->redrawControl('messages');
            $this->redrawControl('messageForm');
            $this->redrawControl('groupName');
            $this->redrawControl('sidePanelComponent');
            $this->redrawControl('sidePanel');
        };

        return $form;
    }

    protected function createComponentAddMessageGroupForm(
        MessageGroupFormFactory $messageGroupFormFactory
    ): MessageGroupForm
    {
        $form = $messageGroupFormFactory->create($this->userAccount);

        $form->onSend[] = function (UserAccount $userAccount, ArrayHash $values) {
            $messageGroup = $this->messageGroupFacade->createNewMessageGroupFromForm($userAccount, $values);

            $this->flashMessage("messageGroupWasSuccessfullyCreated");
            $this->redirect("detail", ['messageGroupId' => $messageGroup->getId()]);
        };

        return $form;
    }

    protected function createComponentSettingsForm(SettingsFormFactory $settingsFormFactory): SettingsForm
    {
        $form = $settingsFormFactory->create($this->userAccount);

        $form->onSend[] = function (UserAccount $userAccount, ArrayHash $values) {
            $this->userAccountService->updateUserAccountData($userAccount, $values);

            $this->flashMessage("userAccountDataWasSuccessfullyChange");
            $this->redirect("this", ['id' => $userAccount->getId()]);
        };

        return $form;
    }

    protected function createComponentMessageGroupSettingsForm(
        MessageGroupSettingsFormFactory $messageGroupFormFactory
    ): MessageGroupSettingsForm
    {
        $form = $messageGroupFormFactory->create($this->messageGroup, $this->userAccount);

        $form->onSend[] = function (UserAccount $userAccount, MessageGroup $messageGroup, ArrayHash $values) {
            $messageGroup = $this->messageGroupFacade->updateMessageGroup($userAccount, $messageGroup, $values);

            if (!$messageGroup->findUserAccount($this->userAccount)) {
                $this->flashMessage("messageGroupWasSuccessfullyUpdatedExtended");
                $this->redirect("default");
            } else {
                $this->flashMessage("messageGroupWasSuccessfullyUpdated");
                $this->redirect("detail", ['messageGroupId' => $messageGroup->getId()]);
            }
        };

        return $form;
    }

}