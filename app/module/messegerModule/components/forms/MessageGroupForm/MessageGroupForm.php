<?php declare(strict_types=1);

namespace App\MessengerModule\Components;

use App\Model\Entity\UserAccount;
use App\Model\Facade\UserAccountFacade;
use DKing\Base\Module\BaseForm;
use DKing\Base\Module\FormFactory;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;

interface MessageGroupFormFactory
{

    public function create(UserAccount $userAccount): MessageGroupForm;

}
class MessageGroupForm extends BaseForm
{

    public const MESSAGE_GROUP_FORM_ID = 'frm-addMessageGroupForm-form-message';

    private UserAccount $userAccount;
    private UserAccountFacade $userAccountFacade;

    //////////////////////////////////////////////////////// Construct

    public function __construct(
        FormFactory $formFactory,
        UserAccount $userAccount,
        UserAccountFacade $userAccountFacade
    )
    {
        parent::__construct($formFactory);

        $this->userAccount = $userAccount;
        $this->userAccountFacade = $userAccountFacade;
    }

    //////////////////////////////////////////////////////// Renders

    public function render(): void
    {
        $this->template->setFile(__DIR__ . '/messageGroupForm.latte');
        $this->template->render();
    }

    //////////////////////////////////////////////////////// Create

    public function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addText('messageGroupName', 'messageGroupName')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'messageGroupMaxNameText', 20);

        $allUserAccounts = $this->userAccountFacade->getAllUserAccountsForMessageGroup($this->userAccount);
        $form->addMultiSelect('selectUsersForMessageGroup', 'selectUsersForMessageGroup', $allUserAccounts)
            ->setRequired();

        $form->addSubmit('send');

        return $form;
    }

    /////////////////////////////////////////////////////// Form values processing

    public function formValuesProcessing(Form $form, ArrayHash $values): void
    {
        $this->onSend($this->userAccount, $values);
    }

}
