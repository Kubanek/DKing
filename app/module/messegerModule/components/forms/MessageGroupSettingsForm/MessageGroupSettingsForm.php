<?php declare(strict_types=1);

namespace App\MessengerModule\Components;

use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use App\Model\Facade\UserAccountFacade;
use DKing\Base\Module\BaseForm;
use DKing\Base\Module\FormFactory;
use Nette\ComponentModel\IComponent;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;

interface MessageGroupSettingsFormFactory
{

    public function create(MessageGroup $messageGroup, UserAccount $userAccount): MessageGroupSettingsForm;

}
class MessageGroupSettingsForm extends BaseForm
{

    public const MESSAGE_GROUP_SETTINGS_FORM_ID = 'frm-messageGroupSettingsForm-form';

    private UserAccount $userAccount;
    private MessageGroup $messageGroup;

    private UserAccountFacade $userAccountFacade;

    //////////////////////////////////////////////////////// Construct

    public function __construct(
        FormFactory $formFactory, 
        UserAccount $userAccount, 
        MessageGroup $messageGroup,
        UserAccountFacade $userAccountFacade
    )
    {
        parent::__construct($formFactory);

        $this->userAccount = $userAccount;
        $this->messageGroup = $messageGroup;
        $this->userAccountFacade = $userAccountFacade;
    }

    //////////////////////////////////////////////////////// Renders

    public function render(): void
    {
        $this->template->setFile(__DIR__ . '/messageGroupSettingsForm.latte');
        $this->template->render();
    }
    
    //////////////////////////////////////////////////////// Create

    public function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addText('messageGroupName', 'messageGroupNameChange')
            ->setRequired()
            ->setDefaultValue($this->messageGroup->getName())
            ->addRule(Form::MAX_LENGTH, 'messageGroupMaxNameText', 20);

        $allUserAccounts = $this->userAccountFacade->getAllUserAccountsForMessageGroup($this->userAccount, false);
        $currentUsers = $this->userAccountFacade->getAllUserAccountIdsForCurrentMessageGroup($this->messageGroup);
        $form->addMultiSelect('selectUsersForMessageGroup', 'selectUsersForMessageGroup', $allUserAccounts)
            ->setDefaultValue($currentUsers);

        $form->addSubmit('send');

        return $form;
    }

    //////////////////////////////////////////////////////// Validations

    public function isEmailFreeToAssign(IComponent $component): bool
    {
        $email = $component->getValue();

        return $this->userAccountFacade->isEmailFreeToAssign($email);
    }

    public function passwordPatternValidation(IComponent $component): bool
    {
        $password = $component->getValue();

        return $this->userAccountFacade->passwordPatternValidation($password);
    }

    /////////////////////////////////////////////////////// Form values processing

    public function formValuesProcessing(Form $form, ArrayHash $values): void
    {
        $this->onSend($this->userAccount, $this->messageGroup, $values);
    }

}
