<?php declare(strict_types=1);

namespace App\FrontModule\Components;

use App\Model\Entity\UserAccount;
use App\Model\Facade\UserAccountFacade;
use DKing\Base\Module\BaseForm;
use DKing\Base\Module\FormFactory;
use Nette\ComponentModel\IComponent;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;

interface SettingsFormFactory
{

    public function create(UserAccount $userAccount): SettingsForm;

}
class SettingsForm extends BaseForm
{

    public const SETTINGS_GROUP_FORM_ID = 'frm-settingsForm-form';

    private UserAccount $userAccount;
    private UserAccountFacade $userAccountFacade;

    //////////////////////////////////////////////////////// Construct

    public function __construct(
        FormFactory $formFactory, 
        UserAccount $userAccount, 
        UserAccountFacade $userAccountFacade
    )
    {
        parent::__construct($formFactory);

        $this->userAccount = $userAccount;
        $this->userAccountFacade = $userAccountFacade;
    }

    //////////////////////////////////////////////////////// Renders

    public function render(): void
    {
        $this->template->setFile(__DIR__ . '/settingsForm.latte');
        $this->template->render();
    }
    
    //////////////////////////////////////////////////////// Create

    public function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addEmail('email', 'email')
            ->setDefaultValue($this->userAccount->getEmail())
            ->setRequired()
            ->addConditionOn($form['email'],Form::NOT_EQUAL, $this->userAccount->getEmail())
            ->addRule([$this, 'isEmailFreeToAssign'], 'emailIsUsed');
        
        $form->addText('name', 'name')
            ->setDefaultValue($this->userAccount->getName())
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'nameMaxLengthText', UserAccount::USER_ACCOUNT_MAX_NAME_LENGTH);

        $form->addText('surname', 'surname')
            ->setDefaultValue($this->userAccount->getSurname())
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'surnameMaxLengthText', UserAccount::USER_ACCOUNT_MAX_NAME_LENGTH);

        $minLength = UserAccount::USER_ACCOUNT_MIN_PASSWORD_LENGTH;
        $maxLength = UserAccount::USER_ACCOUNT_MAX_PASSWORD_LENGTH;
        $form->addPassword('password', 'passwordChange')
            ->addConditionOn($form['password'],Form::NOT_EQUAL, '')
            ->setRequired()
            ->addRule(Form::MIN_LENGTH, 'passwordRangeMinLength', $minLength)
            ->addRule(Form::MAX_LENGTH, 'passwordRangeMaxLength', $maxLength)
            ->addRule([$this, 'passwordPatternValidation'], 'passwordPatternValidationFailed');

        $form->addPassword('passwordVerify', 'passwordVerify')
            ->addRule(Form::EQUAL, 'passwordsDontMatch', $form['password'])
            ->setOmitted();

        $form->addSubmit('send');

        return $form;
    }

    //////////////////////////////////////////////////////// Validations

    public function isEmailFreeToAssign(IComponent $component): bool
    {
        $email = $component->getValue();

        return $this->userAccountFacade->isEmailFreeToAssign($email);
    }

    public function passwordPatternValidation(IComponent $component): bool
    {
        $password = $component->getValue();

        return $this->userAccountFacade->passwordPatternValidation($password);
    }

    /////////////////////////////////////////////////////// Form values processing

    public function formValuesProcessing(Form $form, ArrayHash $values): void
    {
        $this->onSend($this->userAccount, $values);
    }

}
