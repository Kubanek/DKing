<?php declare(strict_types=1);

namespace App\MessengerModule\Components;

use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use DKing\Base\Module\BaseForm;
use DKing\Base\Module\FormFactory;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;

interface MessageFormFactory
{

    public function create(UserAccount $userAccount, MessageGroup $messageGroup): MessageForm;

}
class MessageForm extends BaseForm
{

    public const MESSAGE_FORM_ID = 'frm-messageForm-form';

    private UserAccount $userAccount;

    private MessageGroup $messageGroup;

    //////////////////////////////////////////////////////// Construct

    public function __construct(FormFactory $formFactory, UserAccount $userAccount, MessageGroup $messageGroup)
    {
        parent::__construct($formFactory);

        $this->userAccount = $userAccount;
        $this->messageGroup = $messageGroup;
    }

    //////////////////////////////////////////////////////// Renders

    public function render(): void
    {
        $this->template->setFile(__DIR__ . '/messageForm.latte');
        $this->template->render();
    }

    //////////////////////////////////////////////////////// Create

    public function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addText('message', 'message')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'messageMaxLengthText', 999);

        $form->addSubmit('send');

        return $form;
    }

    /////////////////////////////////////////////////////// Form values processing

    public function formValuesProcessing(Form $form, ArrayHash $values): void
    {
        $this->onSend($this->userAccount, $this->messageGroup, $values->message);
    }

}
