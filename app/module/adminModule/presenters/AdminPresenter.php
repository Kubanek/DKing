<?php declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\BaseModule\Presenters\BasePresenter;
use App\Model\Entity\UserAccount;
use App\Security\Authenticator;

abstract class AdminPresenter extends BasePresenter
{

    public UserAccount $userAccount;

    public function startup(): void
    {
        parent::startup();

        $this->isUserLoggedIn();
    }

    //////////////////////////////////////////////////////// Private

    private function isUserLoggedIn(): void
    {
        $user = $this->getUser();

        if (!$user->isLoggedIn() || !($this->getUser()->getUserAccount() instanceof UserAccount)) {
            $this->flashMessage('youMustSignIn');
            $this->redirect(':Front:Sign:In');
        }

        if (!in_array(Authenticator::ADMIN_ROLE, $this->getUser()->getRoles())) {
            $this->flashMessage('notAllowed');
            $this->redirect(':Front:Sign:In');
        }

        $this->userAccount = $this->getUser()->getUserAccount();
    }

}
