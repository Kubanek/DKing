<?php declare(strict_types=1);

namespace App\CronModule\Presenters;

use App\Service\CronTasksService;
use Nette\Application\Responses\TextResponse;
use stekycz\Cronner\Cronner;

final class TasksPresenter extends CronPresenter
{

    #[Inject]
    public Cronner $cronner;

    #[Inject]
    public CronTasksService $cronTasksService;

    public function startup(): void
    {
        parent::startup();

        if ($this->getParameter('token') !== $this->neon->tokens->cronTasksPresenter) {
            $this->sendResponse(new TextResponse('Provide valid token'));
        }
    }

    //////////////////////////////////////////////////////// Actions

    public function actionDefault(): void
    {
        $this->cronner->addTasks($this->cronTasksService);
        $this->cronner->run();

        $this->sendResponse(new TextResponse('ok'));
    }

}
