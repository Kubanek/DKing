<?php declare(strict_types=1);

namespace App\FrontModule\Components;

use App\Model\Entity\UserAccount;
use App\Service\G2FAuthenticator;
use DKing\Base\Module\BaseForm;
use DKing\Base\Module\FormFactory;
use Nette\ComponentModel\IComponent;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;

interface G2faCheckerFormFactory
{

    public function create(UserAccount $userAccount): G2faCheckerForm;

}
class G2faCheckerForm extends BaseForm
{

    public const G2FA_CHECKER_FORM_ID = 'frm-g2faChecker-form';

    private UserAccount $userAccount;

    private G2FAuthenticator $g2FAuthenticator;

    //////////////////////////////////////////////////////// Construct

    public function __construct(FormFactory $formFactory, UserAccount $userAccount, G2FAuthenticator $g2FAuthenticator)
    {
        parent::__construct($formFactory);

        $this->userAccount = $userAccount;
        $this->g2FAuthenticator = $g2FAuthenticator;
    }

    //////////////////////////////////////////////////////// Renders

    public function render(): void
    {
        $this->template->setFile(__DIR__ . '/g2faCheckerForm.latte');
        $this->template->render();
    }

    //////////////////////////////////////////////////////// Create

    public function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addText('g2faCode', 'g2faCode')
            ->setRequired()
            ->addRule(Form::PATTERN, 'valueMustBeInteger', ".*[0-9].*")
            ->addRule(Form::LENGTH, 'g2faCodeLengthText', 6)
            ->addRule([$this, 'isG2faSecretValide'], 'g2faSecretNotValid');

        $form->addSubmit('send');

        return $form;
    }

    //////////////////////////////////////////////////////// Validations

    public function isG2faSecretValide(IComponent $component): bool
    {
        return $this->g2FAuthenticator->isSecretVerified($this->userAccount, $component->getValue());
    }

    /////////////////////////////////////////////////////// Form values processing

    public function formValuesProcessing(Form $form, ArrayHash $values): void
    {
        $this->onSend($this->userAccount);
    }

}
