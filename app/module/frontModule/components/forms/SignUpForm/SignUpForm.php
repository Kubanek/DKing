<?php declare(strict_types=1);

namespace App\FrontModule\Components;

use App\Model\Entity\UserAccount;
use App\Model\Facade\UserAccountFacade;
use App\Service\G2FAuthenticator;
use App\Service\PotentialUserAccountSession;
use DKing\Base\Module\BaseForm;
use DKing\Base\Module\FormFactory;
use Nette\ComponentModel\IComponent;
use Nette\Forms\Form;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use Nette\Utils\Json;
use Sonata\GoogleAuthenticator\GoogleQrUrl;

interface SignUpFormFactory
{

    public function create(string $companyName): SignUpForm;

}
class SignUpForm extends BaseForm
{

    public const SIGN_UP_FORM_ID = 'frm-signUpForm-form-email';
    public const SIGN_UP_FORM_G2FA_ID = 'frm-signUpForm-g2fa';

    public const SUBMIT_SIGN_UP_FORM = 'sendForm';
    public const SUBMIT_G2FA = 'sendG2fa';

    private G2FAuthenticator $g2FAuthenticator;

    private PotentialUserAccountSession $potentialUserAccountSession;

    private UserAccountFacade $userAccountFacade;

    private string $companyName;

    //////////////////////////////////////////////////////// Construct

    public function __construct(
        string $companyName,
        FormFactory $formFactory,
        G2FAuthenticator $g2FAuthenticator,
        PotentialUserAccountSession $potentialUserAccountSession,
        UserAccountFacade $userAccountFacade,
    )
    {
        parent::__construct($formFactory);

        $this->companyName = $companyName;
        $this->g2FAuthenticator = $g2FAuthenticator;
        $this->potentialUserAccountSession = $potentialUserAccountSession;
        $this->userAccountFacade = $userAccountFacade;
    }

    //////////////////////////////////////////////////////// Renders

    public function render(): void
    {
        $this->template->setFile(__DIR__ . '/signUpForm.latte');
        $this->template->render();
    }

    public function renderG2fa(): void
    {
        $signUpData = Json::decode($this->potentialUserAccountSession->getSignUpData());

        $this->template->googleQrUrl = GoogleQrUrl::generate(
            $signUpData->email,
            base64_decode($signUpData->secret),
            $this->companyName,
            220
        );

        $this->template->setFile(__DIR__ . '/signUpG2fa.latte');
        $this->template->render();
    }

    //////////////////////////////////////////////////////// Create

    public function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addEmail('email', 'email')
            ->setRequired()
            ->addRule([$this, 'isEmailFreeToAssign'], 'emailIsUsed');

        $form->addText('name', 'name')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'nameMaxLengthText', UserAccount::USER_ACCOUNT_MAX_NAME_LENGTH);

        $form->addText('surname', 'surname')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'surnameMaxLengthText', UserAccount::USER_ACCOUNT_MAX_NAME_LENGTH);

        $minLength = UserAccount::USER_ACCOUNT_MIN_PASSWORD_LENGTH;
        $maxLength = UserAccount::USER_ACCOUNT_MAX_PASSWORD_LENGTH;
        $form->addPassword('password', 'password')
            ->setRequired()
            ->addRule(Form::MIN_LENGTH, 'passwordRangeMinLength', $minLength)
            ->addRule(Form::MAX_LENGTH, 'passwordRangeMaxLength', $maxLength)
            ->addRule([$this, 'passwordPatternValidation'], 'passwordPatternValidationFailed');

        $form->addPassword('passwordVerify', 'passwordVerify')
            ->setRequired()
            ->addRule(Form::EQUAL, 'passwordsDontMatch', $form['password'])
            ->setOmitted();

        $form->addSubmit(self::SUBMIT_SIGN_UP_FORM);

        return $form;
    }

    public function createComponentG2fa(): Form
    {
        $form = $this->createForm();

        $form->addSubmit(self::SUBMIT_G2FA, 'g2faVerify');

        return $form;
    }

    //////////////////////////////////////////////////////// Validations

    public function isEmailFreeToAssign(IComponent $component): bool
    {
        $email = $component->getValue();

        return $this->userAccountFacade->isEmailFreeToAssign($email);
    }

    public function passwordPatternValidation(IComponent $component): bool
    {
        $password = $component->getValue();

        return $this->userAccountFacade->passwordPatternValidation($password);
    }

    /////////////////////////////////////////////////////// Form values processing

    public function formValuesProcessing(Form $form, ArrayHash $values): void
    {
        $submitButtonName = $form->isSubmitted()->getName();

        $redirectDestination = "up";

        if ($submitButtonName === self::SUBMIT_SIGN_UP_FORM) {
            $values->secret = base64_encode($this->g2FAuthenticator->generateSecret());
            $values->password = (new Passwords())->hash($values->password);

            $this->potentialUserAccountSession->createSignUpSession($values);
            $redirectDestination = "g2fa";
        } elseif ($submitButtonName === self::SUBMIT_G2FA) {
            $redirectDestination = "g2faChecker";
        }

        $this->onSend($form, $redirectDestination);
    }

}
