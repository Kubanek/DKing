<?php declare(strict_types=1);

namespace App\FrontModule\Components;

use DKing\Base\Module\BaseForm;
use DKing\Base\Module\FormFactory;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

interface SignInFormFactory
{

    public function create(): SignInForm;

}
class SignInForm extends BaseForm
{

    public const SIGN_IN_FORM_ID = 'frm-signInForm-form';
    public const CHECKER_PASSWORD_GAIN_BY_SYSTEM_FORM_ID = 'frm-checker-checker-passwordGainBySystem';

    //////////////////////////////////////////////////////// Construct

    public function __construct(FormFactory $formFactory)
    {
        parent::__construct($formFactory);
    }

    //////////////////////////////////////////////////////// Renders

    public function render(): void
    {
        $this->template->setFile(__DIR__ . '/signInForm.latte');
        $this->template->render();
    }

    public function renderChecker(): void
    {
        $this->template->setFile(__DIR__ . '/signInFormChecker.latte');
        $this->template->render();
    }

    //////////////////////////////////////////////////////// Create

    public function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addEmail('email', 'email')
            ->setRequired()
            ->addRule(Form::EMAIL);

        $form->addPassword('password', 'password')
            ->setRequired();

        $form->addSubmit('send');

        return $form;
    }

    public function createComponentChecker(): Form
    {
        $form = $this->createForm();

        $form->addPassword('passwordGainBySystem', 'passwordGainBySystem')
            ->setRequired();

        $form->addSubmit('send');

        return $form;
    }

    /////////////////////////////////////////////////////// Form values processing

    public function formValuesProcessing(Form $form, ArrayHash $values): void
    {
        $this->onSend($values);
    }

}
