<?php declare(strict_types=1);

namespace App\FrontModule\Presenters;

use App\FrontModule\Components\G2faCheckerForm;
use App\FrontModule\Components\G2faCheckerFormFactory;
use App\FrontModule\Components\SignInForm;
use App\FrontModule\Components\SignInFormFactory;
use App\FrontModule\Components\SignUpForm;
use App\FrontModule\Components\SignUpFormFactory;
use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use App\Model\Facade\MessageGroupFacade;
use App\Model\Facade\UserAccountFacade;
use App\Model\Facade\WhiteListFacade;
use App\Model\Service\G2FAService;
use App\Model\Service\SignLogService;
use App\Model\Service\UserAccountService;
use App\Model\Service\WhiteListService;
use App\Model\Traits\DynamicParameters;
use App\Model\Traits\UniqueIdentifiers;
use App\Security\Authenticator;
use App\Service\G2FAuthenticator;
use App\Service\PotentialUserAccountSession;
use App\Service\UserAccountVerifyService;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IIdentity;
use Nette\Utils\ArrayHash;
use Nette\Utils\Json;

final class SignPresenter extends FrontPresenter
{

    private const ACTION_SIGN_OUT = 'out';

    /** @var WhiteListFacade @inject */
    public WhiteListFacade $whiteListFacade;

    /** @var UserAccountFacade @inject */
    public UserAccountFacade $userAccountFacade;

    /** @var MessageGroupFacade @inject */
    public MessageGroupFacade $messageGroupFacade;

    /** @var G2FAService @inject */
    public G2FAService $g2FAService;

    /** @var SignLogService @inject */
    public SignLogService $signLogService;

    /** @var UserAccountService @inject */
    public UserAccountService $userAccountService;

    /** @var UserAccountVerifyService @inject */
    public UserAccountVerifyService $userAccountVerifyService;

    /** @var WhiteListService @inject */
    public WhiteListService $whiteListService;

    /** @var G2FAuthenticator @inject */
    public G2FAuthenticator $g2FAuthenticator;

    /** @var PotentialUserAccountSession @inject */
    public PotentialUserAccountSession $potentinalUserAccountSession;

    //////////////////////////////////////////////////////// Start up

    public function startup(): void
    {
        parent::startup();

        $action = $this->getAction();

        if ($this->getUser()->isLoggedIn()) {
            if ($action === self::ACTION_SIGN_OUT) {
                return;
            }

            $this->flashMessage('youAreAllReadySignedIn');

            if (in_array(Authenticator::ADMIN_ROLE, $this->getUser()->getRoles())) {
                $this->redirectToAdmin();
            }
            $this->redirectToMessage();
        }

        if ($this->potentinalUserAccountSession->hasExpired()
            && !in_array($action, PotentialUserAccountSession::ACCESSIBLE_SIGN_ACTION_FOR_NOT_SIGNED)) {
            $this->flashMessage("yourSessionHasExpired");
            $this->redirect("in");
        } elseif (!in_array($action, $this->potentinalUserAccountSession->getAccessibleSignActions())) {
            $this->flashMessage('notAllowed');
            $this->redirectToFront();
        }
    }

    //////////////////////////////////////////////////////// Actions

    public function actionOut(): void
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->getUser()->logout();

            $this->flashMessage('youHaveBeenSignedOut');
        }

        $this->redirectToFront();
    }

    //////////////////////////////////////////////////////// Create

    protected function createComponentSignUpForm(SignUpFormFactory $signUpFormFactory): SignUpForm
    {
        $form = $signUpFormFactory->create($this->neon->companyName);

        $form->onSend[] = function (Form $form, string $destination) {
            if (!$this->potentinalUserAccountSession->hasExpired()) {
                $this->redirect($destination);
            }

            $this->flashMessage("yourSessionHasExpired");
            $this->redirect("up");
        };

        return $form;
    }

    protected function createComponentSignInForm(SignInFormFactory $SignInFormFactory): SignInForm
    {
        $form = $SignInFormFactory->create();

        $form->onSend[] = function (ArrayHash $values) {
            $email = $values->email;

            $this->signLogService->createAndSave($email);

            try {
                $this->getUser()->createUserAccountSession($email, $values->password);

                $this->redirect('g2faChecker');
            } catch (AuthenticationException $e) {
                $this->flashMessage($e->getMessage(), 'alert');
            }
        };

        return $form;
    }

    protected function createComponentChecker(SignInFormFactory $SignInFormFactory): SignInForm
    {
        $form = $SignInFormFactory->create();

        $form->onSend[] = function (ArrayHash $values) {
            $passwordGainBySystem = $values->passwordGainBySystem;
            $session = $this->potentinalUserAccountSession;
            $user = $session->getSignInData();

            if (($user instanceof IIdentity) && !$session->hasExpired()) {
                try {
                    $this->userAccountVerifyService->verifyPasswordGainBySystem($user->getId(), $passwordGainBySystem);

                    $this->getUser()->loginFromUserAccountSession($user);
                    $userAccount = $this->userAccountFacade->getUserAccount($user->getId());

                    $whiteList = $this->whiteListService->createFromSession($userAccount);
                    $this->whiteListService->save($whiteList);

                    $this->flashMessage('successfullySignedIn');

                    if (in_array(Authenticator::ADMIN_ROLE, $user->getRoles(), true)) {
                        $this->redirectToAdmin();
                    }
                    $this->redirectToMessage();
                } catch (AuthenticationException $e) {
                    return $this->flashMessage($e->getMessage(), 'alert');
                }
            }

            $this->flashMessage("yourSessionHasExpired");
            $this->redirect("in");
        };

        return $form;
    }

    protected function createComponentG2faCheckerForm(G2faCheckerFormFactory $g2faCheckerFormFactory): G2faCheckerForm
    {
        $userAccount = $this->getUserAccountForG2faCheckerForm();

        $form = $g2faCheckerFormFactory->create($userAccount);

        $form->onSend[] = function (UserAccount $userAccount) {
            $session = $this->potentinalUserAccountSession;
            if (!$session->hasExpired()) {
                $sessionType = $session->getType();
                if ($sessionType === PotentialUserAccountSession::TYPE_SIGN_UP) {
                    $passwordGainBySystem = $userAccount->createPasswordGainBySystem();
                    $userAccount = $this->userAccountService->save($userAccount);

                    $whiteList = $this->whiteListService->createFromSession($userAccount, true);
                    $this->whiteListService->save($whiteList);

                    /** @var $messageGroup MessageGroup */
                    $messageGroup = $this->messageGroupFacade->findOneBy(['isMain' => true]);
                    $this->messageGroupFacade->addUserAccountToCreatedGroup($userAccount, $messageGroup);

                    $session->getSession()->remove();

                    $params = [
                        DynamicParameters::COMPANY_NAME => $this->neon->companyName,
                        DynamicParameters::USER_ACCOUNT_SYSTEM_PASSWORD => $passwordGainBySystem
                    ];

                    try {
                        $this->emailSenderService->sendEmail(
                            UniqueIdentifiers::EMAIL_TEMPLATE_SIGN_UP,
                            $userAccount->getEmail(),
                            $params
                        );
                    } catch (\Exception $exception) {
                        if ($this->developmentEnvironmentChecker->isDevelopement()) {
                            throw new \Exception($exception->getMessage());
                        }
                    }

                    $this->flashMessage('yourAccountWasCreatedPleaseSignIn');
                    $this->redirect('in');
                }

                $user = $session->getSignInData();
                if (!($user instanceof IIdentity)) {
                    $this->flashMessage("yourSessionHasExpired");
                    $this->redirect("in");
                }

                $ipAddress = $this->getHttpRequest()->getRemoteAddress();

                $whiteList = $this->whiteListFacade->findWhiteList($userAccount->getId(), $ipAddress);
                if (!$whiteList) {
                    $this->potentinalUserAccountSession->signInNewIp();
                    $this->redirect("Checker");
                } elseif (!$whiteList->isSignInAllowed()) {
                    $this->redirect('in');
                }

                $this->getUser()->loginFromUserAccountSession($user);

                $this->flashMessage('successfullySignedIn');

                if (in_array(Authenticator::ADMIN_ROLE, $user->getRoles(), true)) {
                    $this->redirectToAdmin();
                }
                $this->redirectToMessage();
            }

            $this->flashMessage("yourSessionHasExpired");
            $this->redirect("in");
        };

        return $form;
    }

    //////////////////////////////////////////////////////// Private

    private function getUserAccountForG2faCheckerForm(): UserAccount
    {
        $session = $this->potentinalUserAccountSession;
        $sessionType = $session->getType();

        $userAccount = $this->userAccountFacade->find($session->getSignInData()?->getId());

        if ($sessionType === PotentialUserAccountSession::TYPE_SIGN_UP) {
            $userAccount = $this->userAccountService->createFromSession(Json::decode($session->getSignUpData()));
            $g2fa = $this->g2FAService->createFromSession(Json::decode($session->getSignUpData()), $userAccount);

            $userAccount->setG2fa($g2fa);
        }

        return $userAccount;
    }

}
