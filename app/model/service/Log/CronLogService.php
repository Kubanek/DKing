<?php declare(strict_types=1);

namespace App\Model\Service;

use App\Model\Entity\CronLog;
use App\Model\Entity\UserAccount;
use DKing\Base\Model\BaseService;
use Nette\Utils\DateTime;

final class CronLogService extends BaseService
{

    //////////////////////////////////////////////////////// Public

    public function create(string $name, UserAccount $userAccount = null): CronLog
    {
        $cronLog = new CronLog;
        $cronLog->setName($name);
        $cronLog->setManualLaunchedBy($userAccount);

        return parent::save($cronLog);
    }

    /**
     * @param CronLog $cronlog
     * @param UserAccount|null $userAccount
     * @return void
     */
    public function save($cronlog, UserAccount $userAccount = null): void
    {
        $currentTimestamp = (new DateTime())->getTimestamp();

        /** @var $dateCreated DateTime */
        $dateCreated = $cronlog->getDateCreated();

        $cronLogTimeStamp = $dateCreated->getTimestamp();

        $cronlog->setDurationInSeconds($currentTimestamp - $cronLogTimeStamp);

        parent::save($cronlog);
    }

}
