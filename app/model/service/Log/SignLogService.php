<?php declare(strict_types=1);

namespace App\Model\Service;

use App\Model\Entity\SignLog;
use DKing\Base\Model\BaseService;
use Nette\Http\IRequest;
use Nettrine\ORM\EntityManagerDecorator;

class SignLogService extends BaseService
{

    private IRequest $request;

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em, IRequest $request)
    {
        parent::__construct($em);

        $this->request = $request;
    }

    //////////////////////////////////////////////////////// Public

    public function createAndSave(string $email): void
    {
        $signLog = new SignLog();

        $signLog->setEmail($email);
        $signLog->setIpAddress($this->request->getRemoteAddress());

        $this->save($signLog);
    }

}
