<?php declare(strict_types=1);

namespace App\Model\Service;

use App\Model\Entity\UserAccount;
use App\Model\Entity\WhiteList;
use DKing\Base\Model\BaseService;
use Nette\Http\IRequest;
use Nettrine\ORM\EntityManagerDecorator;

class WhiteListService extends BaseService
{

    private IRequest $request;

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em, IRequest $request)
    {
        parent::__construct($em);

        $this->request = $request;
    }

    //////////////////////////////////////////////////////// Public

    public function createFromSession(UserAccount $userAccount, bool $isMain = false): WhiteList
    {
        $whiteList = new WhiteList();
        $whiteList->setIpAddress($this->request->getRemoteAddress());
        $whiteList->setLastEditedBy($userAccount);
        $whiteList->setUserAccount($userAccount);
        $whiteList->setSignInAllowed(true);
        $whiteList->setIsMain($isMain);

        return $whiteList;
    }

}
