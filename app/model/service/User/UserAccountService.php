<?php declare(strict_types=1);

namespace App\Model\Service;

use App\Model\Entity\UserAccount;
use DKing\Base\Model\BaseService;
use Nette\Utils\ArrayHash;

class UserAccountService extends BaseService
{

    //////////////////////////////////////////////////////// Public

    public function createFromSession(mixed $values): UserAccount
    {
        $userAccount = new UserAccount();

        $userAccount->setName($values->name);
        $userAccount->setSurname($values->surname);
        $userAccount->setEmail($values->email);
        $userAccount->setPassword($values->password);
        $userAccount->setLastEditedBy($userAccount);

        return $userAccount;
    }

    public function updateUserAccountData(UserAccount $userAccount, ArrayHash $values): UserAccount
    {
        $userAccount->setName($values['name']);
        $userAccount->setSurname($values['surname']);
        $userAccount->setEmail($values['email']);

        if ($values['password'] !== '') {
            $userAccount->setPassword($values['password']);
        }

        $this->save($userAccount, $userAccount);

        return $userAccount;
    }

}
