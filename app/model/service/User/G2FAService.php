<?php declare(strict_types=1);

namespace App\Model\Service;

use App\Model\Entity\G2FA;
use App\Model\Entity\UserAccount;
use DKing\Base\Model\BaseService;

class G2FAService extends BaseService
{

    public function createFromSession(mixed $values, UserAccount $userAccount): G2FA
    {
        $g2fa = new G2FA();
        $g2fa->setSecret($values->secret);
        $g2fa->setUserAccount($userAccount);
        $g2fa->setLastEditedBy($userAccount);

        return $g2fa;
    }
}
