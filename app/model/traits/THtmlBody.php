<?php declare(strict_types=1);

namespace App\Model\Traits;

trait THtmlBody
{

    /** @ORM\Column(type="text", nullable=true) */
    protected ?string $htmlBody = null;

    public function getHtmlBody(): ?string
    {
        return $this->htmlBody;
    }

    public function setHtmlBody(?string $htmlBody): void
    {
        $this->htmlBody = $htmlBody;
    }

}
