<?php declare(strict_types=1);

namespace App\Model\Traits;

trait TIpAddress
{

    /** @ORM\Column(type="string", nullable=true) */
    protected ?string $ipAddress = null;

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(string $email): void
    {
        $this->ipAddress = $email;
    }

}
