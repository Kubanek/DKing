<?php declare(strict_types=1);

namespace App\Model\Traits;

use Doctrine\ORM\Mapping as ORM;

trait IVID
{

    /**
     * @ORM\Id
     * @ORM\Column(type="ivid", length="25")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="DKing\Model\Generator\IvIdGenerator")
     */
    private ?string $id = null;

    final public function getId(): ?string
    {
        return $this->id;
    }

    public function __clone()
    {
        $this->id = NULL;
    }
}
