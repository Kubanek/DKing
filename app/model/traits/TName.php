<?php declare(strict_types=1);

namespace App\Model\Traits;

trait TName
{

    /** @ORM\Column(type="string", nullable=true) */
    protected ?string $name = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

}
