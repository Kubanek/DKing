<?php declare(strict_types=1);

namespace App\Model\Traits;

use Nette\Utils\DateTime;

trait TDateUpdated
{

    /** @ORM\Column(type="datetime") */
    protected ?DateTime $dateUpdated;

    public function getDateUpdated(): ?DateTime
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(?DateTime $dateUpdated): void
    {
        $this->dateUpdated = $dateUpdated;
    }

}
