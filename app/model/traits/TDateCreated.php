<?php declare(strict_types=1);

namespace App\Model\Traits;

use Nette\Utils\DateTime;

trait TDateCreated
{

    /** @ORM\Column(type="datetime") */
    protected ?DateTime $dateCreated = null;

    public function getDateCreated(): ?DateTime
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTime $dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

}
