<?php declare(strict_types=1);

namespace App\Model\Traits;

trait TEmail
{

    /** @ORM\Column(type="string", nullable=true) */
    protected ?string $email = null;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

}
