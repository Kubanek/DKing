<?php declare(strict_types=1);

namespace CZ\DBAL\Traits;

trait FullName
{

	public function getFullName(): string
	{
		return "{$this->getName()} {$this->getSurname()}";
	}

}