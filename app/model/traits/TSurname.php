<?php declare(strict_types=1);

namespace App\Model\Traits;

trait TSurname
{

    /** @ORM\Column(type="string", nullable=true) */
    protected ?string $surname = null;

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $name): void
    {
        $this->surname = $name;
    }

}
