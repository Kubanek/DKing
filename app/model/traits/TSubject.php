<?php declare(strict_types=1);

namespace App\Model\Traits;

trait TSubject
{

    /** @ORM\Column(type="string", nullable=true) */
    protected ?string $subject = null;

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): void
    {
        $this->subject = $subject;
    }

}
