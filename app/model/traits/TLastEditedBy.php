<?php declare(strict_types=1);

namespace App\Model\Traits;

use App\Model\Entity\UserAccount;
use Doctrine\ORM\Mapping as ORM;

trait TLastEditedBy
{

    /**
     * @ORM\ManyToOne(targetEntity="UserAccount")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?UserAccount $lastEditedBy = null;

    public function getLastEditedBy(): ?UserAccount
    {
        return $this->lastEditedBy;
    }

    public function setLastEditedBy(UserAccount $lastEditedBy = null): void
    {
        $this->lastEditedBy = $lastEditedBy;
    }

}
