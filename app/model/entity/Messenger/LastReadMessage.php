<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\TDateCreated;
use App\Model\Traits\TDateUpdated;
use App\Model\Traits\TLastEditedBy;
use App\Model\Traits\TName;
use App\Model\Traits\IVID;
use DKing\Base\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class LastReadMessage extends BaseEntity
{

    use IVID;

    /**
     * @ORM\ManyToOne(targetEntity="UserAccount")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?UserAccount $userAccount;

    /**
     * @ORM\ManyToOne(targetEntity="MessageGroup", inversedBy="messages")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected MessageGroup $messageGroup;

    /**
     * @ORM\ManyToOne(targetEntity="Message")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Message $message = null;

    //////////////////////////////////////////////////////// Getters / Setters

    /**
     * @return UserAccount|null
     */
    public function getUserAccount(): ?UserAccount
    {
        return $this->userAccount;
    }

    /**
     * @param UserAccount|null $userAccount
     */
    public function setUserAccount(?UserAccount $userAccount): void
    {
        $this->userAccount = $userAccount;
    }

    /**
     * @return MessageGroup
     */
    public function getMessageGroup(): MessageGroup
    {
        return $this->messageGroup;
    }

    /**
     * @param MessageGroup $messageGroup
     */
    public function setMessageGroup(MessageGroup $messageGroup): void
    {
        $this->messageGroup = $messageGroup;
    }

    /**
     * @return Message|null
     */
    public function getMessage(): ?Message
    {
        return $this->message;
    }

    /**
     * @param Message|null $message
     */
    public function setMessage(?Message $message): void
    {
        $this->message = $message;
    }

}
