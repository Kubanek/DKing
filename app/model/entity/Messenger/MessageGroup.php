<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\TDateCreated;
use App\Model\Traits\TDateUpdated;
use App\Model\Traits\TLastEditedBy;
use App\Model\Traits\TName;
use App\Model\Traits\IVID;
use DKing\Base\Model\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
class MessageGroup extends BaseEntity
{

    use IVID;
    use TName;
    use TDateCreated;
    use TDateUpdated;
    use TLastEditedBy;

    /**
     * @ORM\ManyToMany(targetEntity="UserAccount", inversedBy="messageGroup")
     * @ORM\JoinTable(name="user_message_groups",
     *   joinColumns={
     *     @ORM\JoinColumn(name="message_group_id", referencedColumnName="id", nullable=true)
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     *   }
     * )
     */
    protected Collection $userAccount;

    /** @ORM\Column(type="boolean") */
    protected bool $isMain = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $now = new DateTime();
        $this->userAccount = new ArrayCollection();

        $this->setDateCreated($now);
        $this->setDateUpdated($now);
    }

    //////////////////////////////////////////////////////// Getters / Setters

    public function addUserAccount(UserAccount $userAccount):void
    {
        $this->userAccount[] = $userAccount;
    }

    public function removeUserAccount(UserAccount $userAccount): void
    {
        $this->userAccount->removeElement($userAccount);
    }

    public function findUserAccount(UserAccount $userAccount): bool
    {
        return $this->userAccount->contains($userAccount);
    }

    public function getUserAccount(): Collection
    {
        return $this->userAccount;
    }

    public function setUserAccount(Collection $userAccount): void
    {
        $this->userAccount = $userAccount;
    }

    public function removeAllUsers(): void
    {
        $this->userAccount = new ArrayCollection();
    }

    public function isMain(): bool
    {
        return $this->isMain;
    }

    public function setIsMain(bool $isMain): void
    {
        $this->isMain = $isMain;
    }

}
