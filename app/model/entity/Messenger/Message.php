<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\TDateCreated;
use App\Model\Traits\TDateUpdated;
use App\Model\Traits\TLastEditedBy;
use App\Model\Traits\TName;
use App\Model\Traits\IVID;
use DKing\Base\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Message extends BaseEntity
{

    use IVID;

    /**
     * @ORM\ManyToOne(targetEntity="UserAccount")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?UserAccount $userAccount;

    /** @ORM\Column(type="text") */
    protected string $text;

    /**
     * @ORM\ManyToOne(targetEntity="MessageGroup", inversedBy="messages")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected MessageGroup $messageGroup;

    //////////////////////////////////////////////////////// Getters / Setters

    public function getUserAccount(): ?UserAccount
    {
        return $this->userAccount;
    }

    public function setUserAccount(UserAccount $userAccount): void
    {
        $this->userAccount = $userAccount;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getMessageGroup(): MessageGroup
    {
        return $this->messageGroup;
    }

    public function setMessageGroup(MessageGroup $messageGroup): void
    {
        $this->messageGroup = $messageGroup;
    }

}
