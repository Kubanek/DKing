<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\THtmlBody;
use App\Model\Traits\TDateUpdated;
use App\Model\Traits\TLastEditedBy;
use App\Model\Traits\TDateCreated;
use App\Model\Traits\IVID;
use App\Model\Traits\TSubject;
use DKing\Base\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Json;

/**
 * @ORM\Entity
 */
class EmailTemplate extends BaseEntity
{

    use IVID;
    use THtmlBody;
    use TDateCreated;
    use TDateUpdated;
    use TLastEditedBy;
    use TSubject;

    /** @ORM\Column(type="string", unique=true) */
    protected string $uniqueIdentifier;

    /** @ORM\Column(type="json", nullable=true) */
    protected ?string $dynamicParameters = null;

    //////////////////////////////////////////////////////// Getters / Setters

    public function getUniqueIdentifier(): string
    {
        return $this->uniqueIdentifier;
    }

    public function setUniqueIdentifier($uniqueIdentifier): void
    {
        $this->uniqueIdentifier = $uniqueIdentifier;
    }

    public function getDynamicParameters(): ?string
    {
        return $this->dynamicParameters;
    }

    public function setDynamicParameters(?array $dynamicParameters = null): void
    {
        if ($dynamicParameters) {
            $dynamicParameters = Json::encode($dynamicParameters);
        }

        $this->dynamicParameters = $dynamicParameters;
    }

}
