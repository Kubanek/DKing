<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\THtmlBody;
use App\Model\Traits\TDateCreated;
use App\Model\Traits\IVID;
use App\Model\Traits\TSubject;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
class EmailLog
{

    use IVID;
    use THtmlBody;
    use TDateCreated;
    use TSubject;

    /**
     * @ORM\ManyToOne(targetEntity="EmailTemplate")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected EmailTemplate $emailTemplate;

    /** @ORM\Column(type="string", nullable=true) */
    protected null|string $emailFrom;

    /** @ORM\Column(type="string", nullable=true) */
    protected null|string $emailTo;

    /** @ORM\Column(type="boolean") */
    protected bool $isSent = false;

    //////////////////////////////////////////////////////// Construct

    public function __construct()
    {
        $this->setDateCreated(new DateTime());
    }

    //////////////////////////////////////////////////////// Getters / Setters

    public function getEmailTemplate(): EmailTemplate
    {
        return $this->emailTemplate;
    }

    public function setEmailTemplate(EmailTemplate $emailTemplate): void
    {
        $this->emailTemplate = $emailTemplate;
    }

    public function getEmailFrom(): ?string
    {
        return $this->emailFrom;
    }

    public function setEmailFrom(?string $emailFrom): void
    {
        $this->emailFrom = $emailFrom;
    }

    public function getEmailTo(): ?string
    {
        return $this->emailTo;
    }

    public function setEmailTo(?string $emailTo): void
    {
        $this->emailTo = $emailTo;
    }

    public function isSent(): bool
    {
        return $this->isSent;
    }

    public function setIsSent(bool $isSent): void
    {
        $this->isSent = $isSent;
    }

}
