<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\TName;
use App\Model\Traits\TDateCreated;
use App\Model\Traits\IVID;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class CronLog
{

    use IVID;
    use TName;
    use TDateCreated;

    /** @ORM\Column(type="integer", nullable=true) */
    protected ?int $durationInSeconds = null;

    /**
     * @ORM\ManyToOne(targetEntity="UserAccount")
     * @ORM\JoinColumn(name="manual_launched_by", referencedColumnName="id")
     */
    protected ?UserAccount $manualLaunchedBy = null;

    //////////////////////////////////////////////////////// Getters / Setters

    public function getDurationInSeconds(): ?int
    {
        return $this->durationInSeconds;
    }

    public function setDurationInSeconds(?int $durationInSeconds): void
    {
        $this->durationInSeconds = $durationInSeconds;
    }

    public function getManualLaunchedBy(): ?UserAccount
    {
        return $this->manualLaunchedBy;
    }

    public function setManualLaunchedBy(?UserAccount $manualLaunchedBy): void
    {
        $this->manualLaunchedBy = $manualLaunchedBy;
    }

}
