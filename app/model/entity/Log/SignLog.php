<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\IVID;
use App\Model\Traits\TEmail;
use App\Model\Traits\TIpAddress;
use Doctrine\ORM\Mapping as ORM;
use App\Model\Traits\TDateCreated;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
class SignLog
{

    use IVID;
    use TDateCreated;
    use TEmail;
    use TIpAddress;

    //////////////////////////////////////////////////////// Construct

    public function __construct()
    {
        $this->setDateCreated(new DateTime());
    }

}
