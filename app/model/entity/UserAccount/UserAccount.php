<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\TEmail;
use App\Model\Traits\TLastEditedBy;
use App\Model\Traits\TName;
use App\Model\Traits\IVID;
use App\Model\Traits\TSurname;
use CZ\DBAL\Traits\FullName;
use DKing\Base\Model\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Security\Passwords;
use Nette\Utils\Random;

/**
 * @ORM\Entity
 */
class UserAccount extends BaseEntity
{

    public const SESSION_POTENTIAL_USER = 'potentialUser';
    public const USER_ACCOUNT_MAX_NAME_LENGTH = 10;
    public const USER_ACCOUNT_MIN_PASSWORD_LENGTH = 8;
    public const USER_ACCOUNT_MAX_PASSWORD_LENGTH = 32;
    public const USER_ACCOUNT_PASSWORD_PATTERN = '[A-z][0-9]';

    use IVID;
    use TName;
    use TSurname;
    use TEmail;
    use TLastEditedBy;
    use FullName;

    /** @ORM\Column(type="string") */
    protected string $password;

    /** @ORM\Column(type="string", nullable=true) */
    protected ?string $passwordGainBySystem = null;

    /** @ORM\OneToOne(targetEntity="G2FA", mappedBy="userAccount", cascade={"persist"}) */
    protected ?G2FA $g2fa = null;

    /** @ORM\Column(type="boolean") */
    protected bool $isAdmin = false;

    /** @ORM\ManyToMany(targetEntity="MessageGroup", mappedBy="userAccount") */
    private Collection $messageGroup;

    //////////////////////////////////////////////////////// Construct

    public function __construct()
    {
        parent::__construct();
        $this->messageGroup = new ArrayCollection();
//        if (!$this->passwordGainBySystem) {
//            $this->createPasswordGainBySystem(); //For safety purposes
//        }
    }

    //////////////////////////////////////////////////////// Utils

    public function getIdentityData(): array
    {
        return [
            'name' => $this->getName(),
            'surname' => $this->getSurname(),
            'email' => $this->getEmail()
        ];
    }

    //////////////////////////////////////////////////////// Public

    public function createPasswordGainBySystem(): string
    {
        $passwordGainBySystem = Random::generate(36);

        $this->setPasswordGainBySystem($passwordGainBySystem);

        return $passwordGainBySystem;
    }

    //////////////////////////////////////////////////////// Getters / Setters

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $passwords = (new Passwords());
        if ($passwords->needsRehash($password)) {
            $password = (new Passwords())->hash($password);
        }

        $this->password = $password;
    }

    public function getPasswordGainBySystem(): ?string
    {
        return $this->passwordGainBySystem;
    }

    public function setPasswordGainBySystem(?string $passwordGainBySystem): void
    {
        $this->passwordGainBySystem = (new Passwords())->hash($passwordGainBySystem);
    }

    public function getG2fa(): ?G2FA
    {
        return $this->g2fa;
    }

    public function setG2fa(?G2FA $g2fa): void
    {
        $this->g2fa = $g2fa;
    }

    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(bool $isAdmin): void
    {
        $this->isAdmin = $isAdmin;
    }

    public function getMessageGroupCollection(): Collection
    {
        return $this->messageGroup;
    }

    public function setMessageGroupCollection(Collection $messageGroup): void
    {
        $this->messageGroup = $messageGroup;
    }

}
