<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\IVID;
use App\Model\Traits\TIpAddress;
use App\Model\Traits\TLastEditedBy;
use DKing\Base\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

//TO DO: Attach it whith RegisterForm
/**
 * @ORM\Entity
 */
class WhiteList extends BaseEntity
{

    use IVID;
    use TIpAddress;
    use TLastEditedBy;

    /**
     * @ORM\ManyToOne(targetEntity="UserAccount")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected UserAccount $userAccount;

    /*FIX ME/TO DO-1: What is better:
    1. user set in admin signInAllowed false => cannot sign in no matter what; when main, then allways be true
    2. user set in admin signInAllowed false => you must do Checker
    */
    /** @ORM\Column(type="boolean") */
    protected bool $signInAllowed = false;

    /** @ORM\Column(type="boolean") */
    protected bool $isMain = false;

    //////////////////////////////////////////////////////// Getters / Setters

    public function getUserAccount()
    {
        return $this->userAccount;
    }

    public function setUserAccount(UserAccount $userAccount): void
    {
        $this->userAccount = $userAccount;
    }

    public function isSignInAllowed(): bool
    {
        return $this->signInAllowed;
    }

    public function setSignInAllowed(bool $signInAllowed): void
    {
        $this->signInAllowed = $signInAllowed;
    }

    public function isMain(): bool
    {
        return $this->isMain;
    }

    public function setIsMain(bool $isMain): void
    {
        $this->isMain = $isMain;
    }

}
