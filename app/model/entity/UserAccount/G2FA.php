<?php declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Traits\IVID;
use App\Model\Traits\TLastEditedBy;
use DKing\Base\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Nette\Security\Passwords;

/**
 * @ORM\Entity
 */
class G2FA extends BaseEntity
{

    use IVID;
    use TLastEditedBy;

    /** @ORM\OneToOne(targetEntity="UserAccount", inversedBy="g2fa") */
    protected UserAccount $userAccount;

    /** @ORM\Column(type="string") */
    protected string $secret;

    //////////////////////////////////////////////////////// Getters / Setters

    public function getUserAccount(): UserAccount
    {
        return $this->userAccount;
    }

    public function setUserAccount(UserAccount $userAccount): void
    {
        $this->userAccount = $userAccount;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function setSecret(string $secret): void
    {
        $this->secret = $secret;
    }

}
