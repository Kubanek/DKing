<?php declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Message;
use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use DKing\Base\Model\BaseRepository;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;

class MessageRepository extends BaseRepository
{

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em, $em->getClassMetadata(Message::class));
    }

    //////////////////////////////////////////////////////// Public

    public function createAndSave(
        UserAccount $userAccount,
        MessageGroup $messageGroup,
        DateTime $now,
        string $text
    ): Message
    {
        $message = new Message();
        $message->setDateCreated($now);
        $message->setDateUpdated($now);
        $message->setMessageGroup($messageGroup);
        $message->setUserAccount($userAccount);
        $message->setText($text);

        $this->save($message);

        return $message;
    }

    //////////////////////////////////////////////////////// Public Queries

    public function getLastMessageByMessageGroupId(string $messageGroupId): ?Message
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select("m")
            ->from(Message::class, 'm')
            ->andWhere($qb->expr()->eq("m.messageGroup", ":messageGroup"))
            ->setParameter(":messageGroup", $messageGroupId)
            ->orderBy('m.dateCreated', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getMessagesByGroupId(string $messageGroupId): array
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select("m")
            ->from(Message::class, 'm')
            ->andWhere($qb->expr()->eq("m.messageGroup", ":messageGroup"))
            ->setParameter(":messageGroup", $messageGroupId)
            ->orderBy('m.dateCreated', 'ASC');

        return $qb->getQuery()->getResult();
    }

}
