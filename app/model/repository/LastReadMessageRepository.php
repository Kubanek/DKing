<?php declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\LastReadMessage;
use App\Model\Entity\Message;
use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use DKing\Base\Model\BaseRepository;
use Nettrine\ORM\EntityManagerDecorator;

class LastReadMessageRepository extends BaseRepository
{

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em, $em->getClassMetadata(LastReadMessage::class));
    }

    //////////////////////////////////////////////////////// Public

    public function createAndSave(
        UserAccount $userAccount,
        MessageGroup $messageGroup,
        Message $message
    ): LastReadMessage
    {
        $lastReadMessage = $this->findOneBy(['userAccount' => $userAccount, 'messageGroup' => $messageGroup]);
        if (!($lastReadMessage instanceof LastReadMessage)) {
            $lastReadMessage = new LastReadMessage();
            $lastReadMessage->setUserAccount($userAccount);
            $lastReadMessage->setMessageGroup($messageGroup);
        }

        $lastReadMessage->setMessage($message);

        $this->save($lastReadMessage);

        return $lastReadMessage;
    }

    //////////////////////////////////////////////////////// Public Queries

    public function getLastMessageByMessageGroupId(string $messageGroupId): ?Message
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select("m")
            ->from(Message::class, 'm')
            ->andWhere($qb->expr()->eq("m.messageGroup", ":messageGroup"))
            ->setParameter(":messageGroup", $messageGroupId)
            ->orderBy('m.dateCreated', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getMessagesByGroupId(string $messageGroupId): array
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select("m")
            ->from(Message::class, 'm')
            ->andWhere($qb->expr()->eq("m.messageGroup", ":messageGroup"))
            ->setParameter(":messageGroup", $messageGroupId)
            ->orderBy('m.dateCreated', 'ASC');

        return $qb->getQuery()->getResult();
    }

}
