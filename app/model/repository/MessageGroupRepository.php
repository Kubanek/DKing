<?php declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Message;
use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use App\Model\Facade\LastReadMessageFacade;
use App\Model\Facade\UserAccountFacade;
use DKing\Base\Model\BaseRepository;
use Nette\Utils\ArrayHash;
use Nettrine\ORM\EntityManagerDecorator;

class MessageGroupRepository extends BaseRepository
{

    public UserAccountFacade $userAccountFacade;
    public LastReadMessageFacade $lastReadMessageFacade;
    public MessageRepository $messageRepository;

    //////////////////////////////////////////////////////// Construct

    public function __construct(
        EntityManagerDecorator $em,
        UserAccountFacade $userAccountFacade,
        LastReadMessageFacade $lastReadMessageFacade,
        MessageRepository $messageRepository
    )
    {
        parent::__construct($em, $em->getClassMetadata(Message::class));

        $this->userAccountFacade = $userAccountFacade;
        $this->lastReadMessageFacade = $lastReadMessageFacade;
        $this->messageRepository = $messageRepository;
    }

    //////////////////////////////////////////////////////// Public

    public function addUserAccountToCreatedGroup(UserAccount $userAccount, MessageGroup $messageGroup): void
    {
        $messageGroup->addUserAccount($userAccount);

        $this->save($messageGroup, $userAccount);
    }

    public function updateMessageGroupLastEditedBy(UserAccount $userAccount, MessageGroup $messageGroup): void
    {
        $this->save($messageGroup, $userAccount);
    }

    public function updateMessageGroup(UserAccount $userAccount, MessageGroup $messageGroup, ArrayHash $values)
    {
        $messageGroup->setName($values['messageGroupName']);
        $messageGroup->removeAllUsers();

        if (count($values['selectUsersForMessageGroup']) > 0) {
            foreach ($values['selectUsersForMessageGroup'] as $userId) {
                $userAccountInGroup = $this->userAccountFacade->getUserAccount($userId);

                $messageGroup->addUserAccount($userAccountInGroup);
                /** @var $lastMessage Message */
                $lastMessage = $this->messageRepository->getLastMessageByMessageGroupId($messageGroup->getId());
                $this->lastReadMessageFacade->createAndSave($userAccount, $messageGroup, $lastMessage);
            }
        }

        $this->save($messageGroup, $userAccount);

        return $messageGroup;
    }

    public function removeUserAccountFromGroup(UserAccount $userAccount, MessageGroup $messageGroup): MessageGroup
    {
        $messageGroup->removeUserAccount($userAccount);

        $this->save($messageGroup, $userAccount);

        return $messageGroup;
    }

}
