<?php declare(strict_types=1);

namespace App\Model\Traits;

class DynamicParameters
{

    //////////////////////////////////////////////////////// EMAIL TEMPLATE
    public const COMPANY_NAME = "__company-name__";
    public const USER_ACCOUNT_SYSTEM_PASSWORD = "__user-account-system-password__";

}
