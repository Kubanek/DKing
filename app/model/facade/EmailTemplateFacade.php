<?php declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\Entity\EmailTemplate;
use DKing\Base\Model\BaseFacade;
use Nettrine\ORM\EntityManagerDecorator;

/**
 * @method EmailTemplate|null findOneBy(array $criteria)
 */
class EmailTemplateFacade extends BaseFacade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em, EmailTemplate::class);
    }

}
