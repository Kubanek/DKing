<?php declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\Entity\EmailLog;
use DKing\Base\Model\BaseFacade;
use Nettrine\ORM\EntityManagerDecorator;

class EmailLogFacade extends BaseFacade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em, EmailLog::class);
    }

}
