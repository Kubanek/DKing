<?php declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use App\Model\Service\UserAccountService;
use DKing\Base\Model\BaseFacade;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use Nettrine\ORM\EntityManagerDecorator;

/**
 * @method UserAccount|null find(string $id = null)
 * @method UserAccount|null findOneBy(array $criteria)
 */
class UserAccountFacade extends BaseFacade
{

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em, UserAccount::class);
    }

    //////////////////////////////////////////////////////// Public

    public function getAllUserAccountsForMessageGroup(UserAccount $userAccount, bool $withoutCurrentUser = true): array
    {
        $resultsInArray = [];
        /** @var $result UserAccount */
        foreach ($this->findAll() as $result) {
            if ($result === $userAccount && $withoutCurrentUser) {
                continue;
            }

            $resultsInArray[(string)($result->getId())] = $result->getFullName();
        }

        return $resultsInArray;
    }

    public function getAllUserAccountIdsForCurrentMessageGroup(MessageGroup $messageGroup): array
    {
        $resultsInArray = [];
        /** @var $result UserAccount */
        foreach ($messageGroup->getUserAccount() as $result) {
            $id = $result->getId();

            $resultsInArray[(string)($id)] = $id;
        }

        return $resultsInArray;
    }

    public function getUserAccount($id): UserAccount
    {
        return $this->find($id);
    }

    //////////////////////////////////////////////////////// Utils

    public function isEmailFreeToAssign($email): bool
    {
        if (!$this->findOneBy(['email' => $email])) {
            return true;
        }

        return false;
    }

    public function passwordPatternValidation(string $password): bool
    {
        $passwordPattern = UserAccount::USER_ACCOUNT_PASSWORD_PATTERN;
        if (Strings::match($password, "~$passwordPattern~")) {
            return true;
        }

        return false;
    }

}
