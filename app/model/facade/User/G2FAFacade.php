<?php declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\Entity\G2FA;
use DKing\Base\Model\BaseFacade;
use Nettrine\ORM\EntityManagerDecorator;

class G2FAFacade extends BaseFacade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em, G2FA::class);
    }

}
