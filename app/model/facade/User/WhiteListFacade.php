<?php declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\Entity\WhiteList;
use DKing\Base\Model\BaseFacade;
use Nettrine\ORM\EntityManagerDecorator;

/**
 * @method WhiteList|null find(string $id = null)
 * @method WhiteList|null findOneBy(array $criteria)
 */
class WhiteListFacade extends BaseFacade
{

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em, WhiteList::class);
    }

    //////////////////////////////////////////////////////// Public

    public function findWhiteList(string $userAccountId, string $ipAddress): ?WhiteList
    {
        return $this->findOneBy(
            [
                'userAccount' => $userAccountId,
                'ipAddress' => $ipAddress
            ]
        );
    }

}
