<?php declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\Entity\LastReadMessage;
use App\Model\Entity\Message;
use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use App\Model\Repository\LastReadMessageRepository;
use DKing\Base\Model\BaseFacade;
use Nettrine\ORM\EntityManagerDecorator;

class LastReadMessageFacade extends BaseFacade
{

    protected LastReadMessageRepository $lastReadMessageRepository;

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em, LastReadMessageRepository $lastReadMessageRepository)
    {
        parent::__construct($em, LastReadMessage::class);
        $this->lastReadMessageRepository = $lastReadMessageRepository;
    }

    //////////////////////////////////////////////////////// Public

    public function getLastReadMessagesForGroup(UserAccount $userAccount,): array
    {
        $lastReadMessages = $this->findBy(['userAccount' => $userAccount]);
        $lastReadMessagesArray = [];

        /** @var $lastReadMessage LastReadMessage */
        foreach ($lastReadMessages as $lastReadMessage) {
            $lastReadMessagesArray[$lastReadMessage->getMessageGroup()->getId()] = $lastReadMessage->getMessage();
        }

        return $lastReadMessagesArray;
    }

    public function createAndSave(
        UserAccount $userAccount,
        MessageGroup $messageGroup,
        Message $message
    ): LastReadMessage
    {
        return $this->lastReadMessageRepository->createAndSave($userAccount, $messageGroup, $message);
    }

}
