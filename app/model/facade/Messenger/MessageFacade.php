<?php declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\Entity\Message;
use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use App\Model\Repository\MessageRepository;
use DKing\Base\Model\BaseFacade;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;

/**
 * @method Message|null findBy(array $criteria, array $order = null, int $limit = null, int $offset = null)
 */
class MessageFacade extends BaseFacade
{

    public MessageRepository $messageRepository;

    //////////////////////////////////////////////////////// Construct

    public function __construct(EntityManagerDecorator $em, MessageRepository $messageRepository)
    {
        parent::__construct($em, Message::class);
        $this->messageRepository = $messageRepository;
    }

    //////////////////////////////////////////////////////// Public

    public function createAndSave(
        UserAccount $userAccount,
        MessageGroup $messageGroup,
        DateTime $now,
        string $text
    ): Message
    {
        return $this->messageRepository->createAndSave($userAccount, $messageGroup, $now, $text);
    }

}
