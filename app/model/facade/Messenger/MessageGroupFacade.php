<?php declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\Entity\MessageGroup;
use App\Model\Entity\UserAccount;
use App\Model\Repository\MessageGroupRepository;
use DKing\Base\Model\BaseFacade;
use Nette\Utils\ArrayHash;
use Nettrine\ORM\EntityManagerDecorator;

/**
 * @method MessageGroup|null find(string $id = null)
 * @method MessageGroup|null findBy(array $criteria, array $order = null, int $limit = null, int $offset = null)
 * @method MessageGroup|null findOneBy(array $array)
 */
class MessageGroupFacade extends BaseFacade
{

    public UserAccountFacade $userAccountFacade;
    public MessageGroupRepository $messageGroupRepository;

    public function __construct(
        EntityManagerDecorator $em,
        MessageGroupRepository $messageGroupRepository,
        UserAccountFacade $userAccountFacade
    )
    {
        parent::__construct($em, MessageGroup::class);
        $this->messageGroupRepository = $messageGroupRepository;
        $this->userAccountFacade = $userAccountFacade;
    }

    //////////////////////////////////////////////////////// Public

    public function addUserAccountToCreatedGroup(UserAccount $userAccount, MessageGroup $messageGroup): void
    {
        $this->messageGroupRepository->addUserAccountToCreatedGroup($userAccount, $messageGroup);
    }

    public function createNewMessageGroupFromForm(UserAccount $userAccount, ArrayHash $values): MessageGroup
    {
        $messageGroupName = $values['messageGroupName'];
        $usersId = $values['selectUsersForMessageGroup'];

        $messageGroup = new MessageGroup();
        $messageGroup->setName($messageGroupName);
        $messageGroup->addUserAccount($userAccount);

        foreach ($usersId as $userId) {
            $messageGroup->addUserAccount($this->userAccountFacade->getUserAccount($userId));
        }

        return $this->messageGroupRepository->save($messageGroup, $userAccount);
    }

    public function updateMessageGroupLastEditedBy(UserAccount $userAccount, MessageGroup $messageGroup): void
    {
        $this->messageGroupRepository->updateMessageGroupLastEditedBy($userAccount, $messageGroup);
    }

    public function updateMessageGroup(
        UserAccount $userAccount,
        MessageGroup $messageGroup,
        ArrayHash $values
    ): MessageGroup
    {
        return $this->messageGroupRepository->updateMessageGroup($userAccount, $messageGroup, $values);
    }

    public function removeUserAccountFromGroup(UserAccount $userAccount, MessageGroup $messageGroup): MessageGroup
    {
        return $this->messageGroupRepository->removeUserAccountFromGroup($userAccount, $messageGroup);
    }

}
