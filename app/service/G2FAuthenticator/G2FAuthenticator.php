<?php declare(strict_types=1);

namespace App\Service;

use App\Model\Entity\UserAccount;
use Sonata\GoogleAuthenticator\GoogleAuthenticator;

class G2FAuthenticator
{

    private GoogleAuthenticator $googleAuthenticator;

    //////////////////////////////////////////////////////// Construct

    public function __construct()
    {
        $this->googleAuthenticator = new GoogleAuthenticator();
    }

    //////////////////////////////////////////////////////// Public

    public function generateSecret(): string
    {
        return $this->googleAuthenticator->generateSecret();
    }

    public function isSecretVerified(UserAccount $userAccount, string $userInput): bool
    {
        return $this->googleAuthenticator->checkCode(
            base64_decode($userAccount->getG2fa()?->getSecret()),
            $userInput
        );
    }

}
