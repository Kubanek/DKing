<?php declare(strict_types=1);

namespace App\Security;

use App\Model\Facade\UserAccountFacade;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;
use Nette\Security\SimpleIdentity;

class Authenticator implements \Nette\Security\Authenticator
{

    public const ADMIN_ROLE = "admin";
    public const USER_ROLE = "user";

    private Passwords $passwords;

    private UserAccountFacade $userAccountFacade;

    //////////////////////////////////////////////////////// Construct

    public function __construct(Passwords $passwords, UserAccountFacade $userAccountFacade)
    {
        $this->passwords = $passwords;
        $this->userAccountFacade = $userAccountFacade;
    }

    //////////////////////////////////////////////////////// Public

    public function authenticate(string $email, string $password): SimpleIdentity
    {
        $userAccount = $this->userAccountFacade->findOneBy(['email' => $email]);

        if (!$userAccount) {
            throw new AuthenticationException('wrongCredentials', self::IDENTITY_NOT_FOUND);
        }

        if (!$this->passwords->verify($password , $userAccount->getPassword())) {
            throw new AuthenticationException('wrongCredentials', self::INVALID_CREDENTIAL);
        }

        $role = $userAccount->isAdmin() ? self::ADMIN_ROLE: self::USER_ROLE;

        return new SimpleIdentity($userAccount->getId(), $role, $userAccount->getIdentityData());
    }

}
