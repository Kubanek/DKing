<?php declare(strict_types=1);

namespace App\Security;

use App\Model\Entity\UserAccount;
use App\Model\Facade\UserAccountFacade;
use App\Service\PotentialUserAccountSession;
use Nette\Security\IIdentity;
use Nette\Security\UserStorage;

/**
 * @method onLoggedIn(User $user)
 */
class User extends \Nette\Security\User
{

    private UserAccountFacade $userAccountFacade;

    private UserStorage $storage;

    private PotentialUserAccountSession $potentinalUserAccountSession;

    //////////////////////////////////////////////////////// Construct

    public function __construct(
        Authenticator $authenticator,
        UserStorage $storage,
        PotentialUserAccountSession $potentinalUserAccountSession,
        UserAccountFacade $userAccountFacade
    ) {
        parent::__construct(
            authenticator: $authenticator,
            storage: $storage
        );

        $this->storage = $storage;
        $this->potentinalUserAccountSession = $potentinalUserAccountSession;
        $this->userAccountFacade = $userAccountFacade;
    }

    //////////////////////////////////////////////////////// Utils

    public function getUserAccount(): ?UserAccount
    {
        return $this->userAccountFacade->find($this->getId());
    }

    //////////////////////////////////////////////////////// Public

    public function login($user, string $password = null): void
    {
        $this->logout(true);
        if (!($user instanceof IIdentity)) {
            if (!$authenticator = $this->getAuthenticator()) {
                return;
            }

            $user = $authenticator->authenticate(...func_get_args());
        }

        $this->storage->saveAuthentication($user);
        $this->onLoggedIn($this);
    }

    public function createUserAccountSession($user, string $password = null): void
    {
        $this->logout(true);
        if (!($user instanceof IIdentity)) {
            if (!$authenticator = $this->getAuthenticator()) {
                return;
            }

            $user = $authenticator->authenticate(...func_get_args());
        }

        $this->potentinalUserAccountSession->createSignInSession($user);
    }

    public function loginFromUserAccountSession(IIdentity $user): void
    {
        $this->storage->saveAuthentication($user);
        $this->potentinalUserAccountSession->getSession()->remove();

        $this->onLoggedIn($this);
    }

}
