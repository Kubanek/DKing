<?php


namespace App\Service;

use Nette\Http\IRequest;

class DevelopmentEnvironmentChecker
{

    private const LOCAL_IP = '127.0.0.1';

    private IRequest $request;

    //////////////////////////////////////////////////////// Construct

    public function __construct(IRequest $request)
    {
        $this->request = $request;
    }

    //////////////////////////////////////////////////////// Public

    public function isDevelopement(): bool
    {
        return $this->request->getRemoteAddress() === self::LOCAL_IP;
    }

}
