<?php declare(strict_types=1);

namespace App\Service;

use App\Model\Service\CronLogService;
use DKing\Base\Model\BaseService;
use Nettrine\ORM\EntityManagerDecorator;

final class CronTasksService extends BaseService
{

    protected EntityManagerDecorator $em;

    private CronLogService $cronLogService;

    public function __construct(EntityManagerDecorator $em, CronLogService $cronLogService)
    {
        parent::__construct($em);
        $this->cronLogService = $cronLogService;
    }

    /**
     * @cronner-task Posílá informaci o zpožděné objednávce
     * @cronner-period 1 minute
     */
    public function sendOrderDelayedEmail()
    {
        $cronLog = $this->cronLogService->create('test');
        $this->cronLogService->save($cronLog);
    }

}
