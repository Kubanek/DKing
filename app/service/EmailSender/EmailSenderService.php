<?php declare(strict_types=1);

namespace App\Service;

use App\Model\Entity\EmailLog;
use App\Model\Facade\EmailTemplateFacade;
use App\Model\Service\EmailLogService;
use App\Model\Traits\DynamicParameters;
use Contributte\Translation\Translator;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Utils\Json;

class EmailSenderService
{

    private EmailTemplateFacade $emailTemplateFacade;

    private EmailLogService $emailLogService;

    private Translator $translator;

    private DevelopmentEnvironmentChecker $developmentEnvironmentChecker;

    private string $companyEmail;

    public function __construct(
        string $companyEmail,
        EmailTemplateFacade $emailTemplateFacade,
        EmailLogService $emailLogService,
        Translator $translator,
        DevelopmentEnvironmentChecker $developmentEnvironmentChecker
    )
    {
        $this->companyEmail = $companyEmail;

        $this->emailTemplateFacade = $emailTemplateFacade;
        $this->emailLogService = $emailLogService;
        $this->translator = $translator;
        $this->developmentEnvironmentChecker = $developmentEnvironmentChecker;
    }

    public function sendEmail(string $uniqueIdentifier, string $to, array $params = [], array $attachment = []): void
    {
        if (!$emailTemplate = $this->emailTemplateFacade->findOneBy(['uniqueIdentifier' => $uniqueIdentifier])) {
            $message = $this->translator->translate('developer.emailTemplateDoesNotExist');

            throw new \Exception("$uniqueIdentifier - $message");
        }

        $dynamicParams = Json::decode($emailTemplate->getDynamicParameters() ?? "[]");
        $this->checkParams($dynamicParams, $params);

        $from = $this->companyEmail;
        $subject = $emailTemplate->getSubject();
        $rawEmailTemplateHtmlBody = $emailTemplate->getHtmlBody();
        $emailTemplateHtmlBody = str_replace($dynamicParams, $params, $rawEmailTemplateHtmlBody);

        $emailLog = new EmailLog();
        $emailLog->setSubject($subject);
        $emailLog->setEmailFrom($from);
        $emailLog->setEmailTo($to);
        $emailLog->setHtmlBody($emailTemplate->getHtmlBody()); //For security purposes is saved only raw "template"
        $emailLog->setHtmlBody($rawEmailTemplateHtmlBody);
        $emailLog->setEmailTemplate($emailTemplate);

        $this->emailLogService->save($emailLog);

        $emailer = new SendmailMailer();

        $email = new Message();
        $email->setFrom($from)
            ->addTo($to)
            ->setSubject($subject)
            ->setHtmlBody($emailTemplateHtmlBody);
//            ->addAttachment($attachment) //FIX-ME: This needs to by modified

        $emailer->send($email);

        $emailLog->setIsSent(true);
        $this->emailLogService->save($emailLog);
    }

    // $dynamicParams = params from EmailTemplate; $params = params inserted in main method
    private function checkParams(array $dynamicParams, array $params): string|bool
    {
        $dynamicParamsSize = count($dynamicParams);
        $paramsSize = count($params);

        if ($dynamicParamsSize !== $paramsSize) {
            throw new \Exception($this->translator->translate('developer.arraysSizeDontMatch'));
        }

        foreach ($params as $key => $param) {
            if (!in_array($key, $dynamicParams)) {
                $message = $this->translator->translate("developer.notFoundInDynamicParams");

                throw new \Exception("$key - $message");
            }
        }

        return false;
    }

}
