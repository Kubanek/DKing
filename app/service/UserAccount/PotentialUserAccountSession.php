<?php declare(strict_types=1);

namespace App\Service;

use App\Model\Entity\UserAccount;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Security\IIdentity;
use Nette\Utils\ArrayHash;
use Nette\Utils\Json;

class PotentialUserAccountSession
{

    public const TYPE_SIGN_UP = 'signUp';
    public const TYPE_SIGN_IN = 'signIn';
    public const TYPE_SIGN_IN_NEW_IP = 'signInNewIp';

    public const ACCESSIBLE_SIGN_ACTION_FOR_NOT_SIGNED = ['in', 'up'];

    private const ACCESSIBLE_SIGN_ACTION_FOR_SIGGNED_UP = ['g2fa', 'g2faChecker'];
    private const ACCESSIBLE_SIGN_ACTION_FOR_SIGGNED_IN = ['g2faChecker'];
    private const ACCESSIBLE_SIGN_ACTION_FOR_SIGGNED_IN_NEW_IP = ['checker'];

    private SessionSection $session;

    //////////////////////////////////////////////////////// Construct

    public function __construct(Session $session, G2FAuthenticator $g2FAuthenticator)
    {
        $this->session = $session->getSection(UserAccount::SESSION_POTENTIAL_USER);

        //$this->g2FAuthenticator = $g2FAuthenticator;
    }

    //////////////////////////////////////////////////////// Create

    public function createSignUpSession(ArrayHash $values): void
    {
        $this->session->setExpiration('30 minutes');
        $this->session->signUp = Json::encode($values);
        $this->session->signIn = null;
        $this->session->type = self::TYPE_SIGN_UP;
    }

    public function createSignInSession(IIdentity $user): void
    {
        $this->session->setExpiration('30 minutes');
        $this->session->signIn = $user;
        $this->session->signUp = null;
        $this->session->type = self::TYPE_SIGN_IN;
    }

    //////////////////////////////////////////////////////// Utils

    public function signInNewIp(): void
    {
        $this->session->type = self::TYPE_SIGN_IN_NEW_IP;
    }

    //////////////////////////////////////////////////////// Public

    public function getSession(): SessionSection
    {
        return $this->session;
    }

    public function getSignUpData(): ?string
    {
        return $this->session->signUp;
    }

    public function getSignInData(): ?IIdentity
    {
        return $this->session->signIn;
    }

    public function getType(): ?string
    {
        return $this->session->type;
    }

    public function hasExpired(): bool
    {
        return !$this->getType();
    }

    public function getAccessibleSignActions(): array
    {
        if ($this->getType() === self::TYPE_SIGN_UP) {
            return array_merge(
                self::ACCESSIBLE_SIGN_ACTION_FOR_NOT_SIGNED,
                self::ACCESSIBLE_SIGN_ACTION_FOR_SIGGNED_UP
            );
        }

        if ($this->getType() === self::TYPE_SIGN_IN) {
            return array_merge(
                self::ACCESSIBLE_SIGN_ACTION_FOR_NOT_SIGNED,
                self::ACCESSIBLE_SIGN_ACTION_FOR_SIGGNED_IN
            );
        }

        if ($this->getType() === self::TYPE_SIGN_IN_NEW_IP) {
            return array_merge(
                self::ACCESSIBLE_SIGN_ACTION_FOR_SIGGNED_IN,
                self::ACCESSIBLE_SIGN_ACTION_FOR_SIGGNED_IN,
                self::ACCESSIBLE_SIGN_ACTION_FOR_SIGGNED_IN_NEW_IP
            );
        }

        return self::ACCESSIBLE_SIGN_ACTION_FOR_NOT_SIGNED;
    }

}
