<?php declare(strict_types=1);

namespace App\Service;

use App\Model\Facade\UserAccountFacade;
use App\Security\Authenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Passwords;

class UserAccountVerifyService
{

    private UserAccountFacade $userAccountFacade;

    private Passwords $passwords;

    //////////////////////////////////////////////////////// Construct

    public function __construct(UserAccountFacade $userAccountFacade, Passwords $passwords)
    {
        $this->userAccountFacade = $userAccountFacade;
        $this->passwords = $passwords;
    }

    //////////////////////////////////////////////////////// Public

    public function verifyPasswordGainBySystem(string $userId, string $passwordGainBySystem): void
    {
        $userAccount = $this->userAccountFacade->getUserAccount($userId);

        if (!$this->passwords->verify($passwordGainBySystem, $userAccount->getPasswordGainBySystem())) {
            throw new AuthenticationException(
                'passwordGainBySystemDoesntMatch',
                Authenticator::INVALID_CREDENTIAL
            );
        }
    }

}
