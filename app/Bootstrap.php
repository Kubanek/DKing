<?php declare(strict_types=1);

use Nette\Utils\Strings;
use Tracy\Debugger;

require __DIR__ . '/../vendor/autoload.php';

Debugger::$showLocation = true;

$configurator = new Nette\Configurator;

//$isVps = isset($_SERVER['SERVER_NAME'])
//    ? Strings::endsWith($_SERVER['SERVER_NAME'], 'vps.incolor.cz')
//    : false;
//
//$configurator->setDebugMode([
//    ($isVps ? $_SERVER['REMOTE_ADDR'] ?? '' : '127.0.0.1')
//]);

$configurator->setDebugMode('127.0.0.1');

$logDir = __DIR__ . '/../log';
$configurator->enableDebugger($logDir);

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
    ->addDirectory(__DIR__)
    ->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
